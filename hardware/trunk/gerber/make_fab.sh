#!/bin/bash
#
# make zip file and template README for this project
# generate gerbers with "use proper filename extensions" UNCHECKED
# so all the gerber file names end in ".pho"
#
base="G2-TDC-INT"
orig="TDC_Mother"
cp ${orig}-B.Mask.gbr ${base}-B_Mask.gbr
cp ${orig}-B.Paste.gbr ${base}-B_Paste.gbr
cp ${orig}-B.SilkS.gbr ${base}-B_SilkS.gbr
cp ${orig}-Cmts.User.gbr ${base}-Fab_Dwg.gbr
cp ${orig}.drl ${base}.drl
cp ${orig}-drl_map.gbr ${base}-drl_map.gbr
cp ${orig}-F.Mask.gbr ${base}-F_Mask.gbr
cp ${orig}-F.Paste.gbr ${base}-F_Paste.gbr
cp ${orig}-F.SilkS.gbr ${base}-F_SilkS.gbr
cp ${orig}-Layer1.gbr ${base}-layer_1.gbr
cp ${orig}-Layer2.gbr ${base}-layer_2.gbr
cp ${orig}-Layer3.gbr ${base}-layer_3.gbr
cp ${orig}-Layer4.gbr ${base}-layer_4.gbr
cp ${orig}-Layer5.gbr ${base}-layer_5.gbr
cp ${orig}-Layer6.gbr ${base}-layer_6.gbr
cp ${orig}-NPTH.drl ${base}-NPTH.drl
cp ${orig}-NPTH-drl_map.gbr ${base}-NPTH-drl_map.gbr
cp ${orig}-bottom.pos ${base}-bottom.pos
cp ${orig}-top.pos ${base}-top.pos

