Boston University P/N G2-TDC-INT Rev B  2015-12-15

6 Layers
Final thickness 0.063 in
Surface finish ENIG

White silkscreen both sides
Green soldermask both sides

G2-TDC-INT-B_Mask.pho		Bottom solder mask
G2-TDC-INT-B_Paste.pho		Bottom solder paste
G2-TDC-INT-B_SilkS.pho		Bottom silkscreen
G2-TDC-INT.drl			NC drill data
G2-TDC-INT-drl_map.pho		NC drill map
G2-TDC-INT-Fab_Dwg.gbr		Fabrication comments
G2-TDC-INT-F_Mask.gbr		Top solder mask
G2-TDC-INT-F_Paste.gbr		Top solder paste
G2-TDC-INT-F_SilkS.gbr		Top silkscreen
G2-TDC-INT-F_SilkS.gbr		Top silkscreen
G2-TDC-INT-layer_1.gbr		Layer 1 0.5 oz (signal)
G2-TDC-INT-layer_2.gbr		Layer 2 0.5 oz (power, positive)
G2-TDC-INT-layer_3.gbr		Layer 3 0.5 oz (signal)
G2-TDC-INT-layer_4.gbr		Layer 4 0.5 oz (signal)
G2-TDC-INT-layer_5.gbr		Layer 5 0.5 oz (power, positive)
G2-TDC-INT-layer_6.gbr		Layer 6 0.5 oz (signal)
G2-TDC-INT-NPTH.drl		NC drill data non-plated holes
G2-TDC-INT-NPTH-drl_map.gbr	NC drill map non-plated holes

