update=Fri 05 Feb 2016 04:38:34 PM EST
last_client=kicad
[eeschema]
version=1
LibDir=lib
[eeschema/libraries]
LibName1=TDC_Mother-rescue
LibName2=power
LibName3=transistors
LibName4=conn
LibName5=display
LibName6=contrib
LibName7=ep3c5u256_custom
LibName8=ERM8-RA
LibName9=30310-5002hb
LibName10=4816P-1-101LF
LibName11=FIN1108
LibName12=tps78630kttt
LibName13=tps79533
LibName14=+4
LibName15=640456-2
LibName16=micron
LibName17=tps73701drbt
LibName18=lib/ws2812b
LibName19=lib/fixed_conn
LibName20=lib/ao3422
LibName21=lib/my_discrete
LibName22=lib/TMM-105-01
LibName23=lib/my_power
LibName24=lib/atmega168a-mm
LibName25=lib/CAT16-101J4LF
LibName26=lib/tmp36
LibName27=lib/testpoint
[schematic_editor]
version=1
PageLayoutDescrFile=worksheet.kicad_wks
PlotDirectoryName=
SubpartIdSeparator=0
SubpartFirstId=65
NetFmtName=
SpiceForceRefPrefix=0
SpiceUseNetNumbers=0
LabSize=60
