
import string


footprint_file = open("ubga-256.kicad_mod", "w")

header = """(module UBGA-256 (layer F.Cu) (tedit 562124C3)
  (fp_text reference UBGA-256 (at 5.9 15.2) (layer F.SilkS)
    (effects (font (size 1.5 1.5) (thickness 0.15)))
  )
  (fp_text value VAL** (at 4.1 -2.7) (layer F.SilkS)
    (effects (font (size 1.5 1.5) (thickness 0.15)))
  )
  (fp_text user 16 (at 12 -1.5) (layer F.SilkS)
    (effects (font (size 0.508 0.508) (thickness 0.0635)))
  )
  (fp_text user T (at -1.6 12) (layer F.SilkS)
    (effects (font (size 0.508 0.508) (thickness 0.0635)))
  )
  (fp_text user A (at -1.6 0) (layer F.SilkS)
    (effects (font (size 0.508 0.508) (thickness 0.0635)))
  )
  (fp_text user 1 (at 0 -1.5) (layer F.SilkS)
    (effects (font (size 0.508 0.508) (thickness 0.0635)))
  )
  (fp_circle (center -2 -2) (end -1.6 -1.7) (layer F.SilkS) (width 0.15))
  (fp_line (start -1 -1) (end -1 13) (layer F.SilkS) (width 0.15))
  (fp_line (start -1 13) (end 13 13) (layer F.SilkS) (width 0.15))
  (fp_line (start 13 13) (end 13 -1) (layer F.SilkS) (width 0.15))
  (fp_line (start 13 -1) (end -1 -1) (layer F.SilkS) (width 0.15))\n"""

footprint_file.write(header)

NUM_ROWS = 16

letter_indicies = "ABCDEFGHJKLMNPRT"

pad_list = []

circle_size = 0.34
solder_mask_margin = 0.07
pad_pitch = 0.8

for row, a in enumerate(letter_indicies):
    for col in range(NUM_ROWS):
        pad_list.append({"Pad" : a + str(col+1), "X" : col*pad_pitch, "Y": row*pad_pitch})

for pad in pad_list:
    string = """ (pad %s smd circle (at %f %f) (size %f %f) (layers F.Cu F.Paste F.Mask)
    (solder_mask_margin %f))""" % (pad["Pad"], pad["X"], pad["Y"],
                                   circle_size, circle_size, solder_mask_margin)
    footprint_file.write(string)

footprint_file.write("\n)")
footprint_file.close()


