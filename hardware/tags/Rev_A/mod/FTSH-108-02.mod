PCBNEW-LibModule-V1  Fri 21 Nov 2014 02:48:14 PM EST
# encoding utf-8
Units mm
$INDEX
FTSH-108-02
$EndINDEX
$MODULE FTSH-108-02
Po 0 0 0 15 546E647D 00000000 ~~
Li FTSH-108-02
Sc 0
AR 
Op 0 0 0
T0 0 -5.35 1 1 0 0.15 N V 21 N "FTSH-108-02"
T1 0 4.69 1 1 0 0.15 N V 21 N "VAL**"
DS -6.365 -2.225 -6.365 2.225 0.15 21
DS 6.365 -2.225 6.365 2.225 0.15 21
$PAD
Sh "1" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.44 1.72
.LocalClearance 0.07
$EndPAD
$PAD
Sh "2" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.44 -1.71
.LocalClearance 0.07
$EndPAD
$PAD
Sh "3" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.17 1.72
.LocalClearance 0.07
$EndPAD
$PAD
Sh "4" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.17 -1.71
.LocalClearance 0.07
$EndPAD
$PAD
Sh "5" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.9 1.72
.LocalClearance 0.07
$EndPAD
$PAD
Sh "6" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.9 -1.71
.LocalClearance 0.07
$EndPAD
$PAD
Sh "7" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.63 1.72
.LocalClearance 0.07
$EndPAD
$PAD
Sh "8" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.63 -1.71
.LocalClearance 0.07
$EndPAD
$PAD
Sh "9" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.64 1.72
.LocalClearance 0.07
$EndPAD
$PAD
Sh "10" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.64 -1.71
.LocalClearance 0.07
$EndPAD
$PAD
Sh "11" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.91 1.72
.LocalClearance 0.07
$EndPAD
$PAD
Sh "12" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.91 -1.71
.LocalClearance 0.07
$EndPAD
$PAD
Sh "13" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.18 1.72
.LocalClearance 0.07
$EndPAD
$PAD
Sh "14" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.18 -1.71
.LocalClearance 0.07
$EndPAD
$PAD
Sh "15" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00A88000
Ne 0 ""
Po 4.45 1.72
.LocalClearance 0.07
$EndPAD
$PAD
Sh "16" R 0.74 2.79 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.45 -1.71
.LocalClearance 0.07
$EndPAD
$EndMODULE FTSH-108-02
$EndLIBRARY
