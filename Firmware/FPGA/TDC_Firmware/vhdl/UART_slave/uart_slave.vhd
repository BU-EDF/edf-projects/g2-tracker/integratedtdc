---------------------------------------------------------------------------------
--uart slave inteface for 4 bit address / 16 bit data registers
---------------------------------------------------------------------------------
-- UART protocol
-- START
--   Master: Write HWAddr[1:0], R/W bit, reserved, register_addr[3:0]
-- READ command
--   Slave: Write Data[15..8]
--   Slave: Write Data[ 7..0]
--   Slave: Write SWAddr[1:0],HWAddr[1:0], register_addr[3:0]
-- WRITE command
--   Master: Write Data[15..8]
--   Master: Write Data[ 7..0]
--   Slave: Write SWAddr[1:0],HWAddr[1:0], register_addr[3:0]

---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

entity uart_slave is
  
  port (
    clk40            : in  std_logic;
    reset            : in  std_logic;
    UART_Rx          : in  std_logic;
    UART_Tx          : out std_logic;
    UART_Tx_en       : out std_logic;

    hw_address       : in  std_logic_vector(1 downto 0);
    sw_address       : in  std_logic_vector(1 downto 0);
    
    register_address : out std_logic_vector(3 downto 0);
    register_data_in          : in  std_logic_vector(15 downto 0);
    register_data_out         : out std_logic_vector(15 downto 0);
    register_data_out_valid   : out std_logic);



end entity uart_slave;


architecture Behaviour of uart_slave is

  component ClkUnit is
    port (
      SysClk   : in  Std_Logic;
      EnableRx : out Std_Logic;
      EnableTx : out Std_Logic;
      Reset    : in  Std_Logic);
  end component ClkUnit;
  
  component RxUnit is
    port (
      Clk    : in  Std_Logic;
      Reset  : in  Std_Logic;
      Enable : in  Std_Logic;
      RxD    : in  Std_Logic;
      RD     : in  Std_Logic;
      FErr   : out Std_Logic;
      OErr   : out Std_Logic;
      DRdy   : out Std_Logic;
      DataIn : out Std_Logic_Vector(7 downto 0));
  end component RxUnit;

  component TxUnit is
    port (
      Clk    : in  Std_Logic;
      Reset  : in  Std_Logic;
      Enable : in  Std_Logic;
      Load   : in  Std_Logic;
      TxD    : out Std_Logic;
      TRegE  : out Std_Logic;
      TBufE  : out Std_Logic;
      DataO  : in  Std_Logic_Vector(7 downto 0));
  end component TxUnit;

  -- signals
  type STATES is (STATE_IDLE ,STATE_START,STATE_READ_LOAD ,STATE_READ_1 ,STATE_READ_2 ,STATE_WRITE_1 ,STATE_WRITE_2 ,STATE_END ,STATE_IGNORE);
  signal current_state : STATES := STATE_IDLE;
  signal read_state_delay : std_logic_vector(7 downto 0) := x"00";
  
  signal clk_enable_rx : std_logic := '0';
  signal clk_enable_tx : std_logic := '0';

  signal state_reset : std_logic := '0';
  signal command_timer : unsigned(23 downto 0) := x"000000";
  signal command_reset_time : unsigned(23 downto 0) := x"200000";
  
  signal rx_data : std_logic_vector(7 downto 0) := x"00";
  signal rx_data_ready : std_logic := '0';
  signal rx_data_rd : std_logic := '0';

  signal tx_data : std_logic_vector(7 downto 0) := x"00";
  signal tx_data_wr : std_logic := '0';
  signal tx_buffer_empty : std_logic := '0';
  signal tx_register_empty : std_logic := '0';
  signal UART_tx_buffer : std_logic := '0';

  signal rw_bit : std_logic;
  signal local_data : std_logic_vector(15 downto 0);
  signal master_ignore : std_logic := '1';
  signal register_address_buffer : std_logic_vector(3 downto 0) := x"0";
  
begin  -- architecture Behaviour

  -- watchdog to kill an open transfer if is has gone too long
  -- 4 8-bit words @ 9600 * safety factor 4-> us
  watchdog: process (clk40, reset) is
  begin  -- process watchdog
    if reset = '1' then                 -- asynchronous reset (active high)
      state_reset <= '1';
    elsif clk40'event and clk40 = '1' then  -- rising clock edge
      state_reset <= '0';
      if current_state /= STATE_IDLE then
        command_timer <= command_timer + 1;
        if command_timer = command_reset_time then
          state_reset <= '1';
        end if;
      else
        command_timer <= x"000000";
      end if;
    end if;
  end process watchdog;


  
  ClkUnit_1: entity work.ClkUnit
    port map (
      SysClk   => clk40,
      EnableRx => clk_enable_rx,
      EnableTx => clk_enable_tx,
      Reset    => state_reset);

  RxUnit_1: entity work.RxUnit
    port map (
      Clk    => clk40,
      Reset  => state_reset,
      Enable => clk_enable_rx,
      RxD    => UART_Rx,
      RD     => rx_data_rd,
      FErr   => open,
      OErr   => open,
      DRdy   => rx_data_ready,
      DataIn => rx_data);

  TxUnit_1: entity work.TxUnit
    port map (
      Clk    => clk40,
      Reset  => state_reset,
      Enable => clk_enable_tx,
      Load   => tx_data_wr,
      TxD    => UART_Tx_buffer,
      TRegE  => tx_register_empty,
      TBufE  => tx_buffer_empty,
      DataO  => tx_data);


  UART_Tx <= UART_Tx_Buffer;
  UART_Tx_en <= not UART_Tx_Buffer;


  register_address <= register_address_buffer;
  -- state machine
  slave_state_machine: process (clk40, state_reset) is
  begin  -- process slave_state_machine
    if reset = '1' then                 -- asynchronous reset (active high)
      current_state <= STATE_IDLE;
      Tx_data_wr <= '0';
      rx_data_rd <= '0';
      register_data_out_valid <= '0';
      register_address_buffer <= x"0";
      rw_bit <= '1';
      master_ignore <= '1';
      local_data <= x"DEAD";
      Tx_data <= x"FF";
    elsif clk40'event and clk40 = '1' then  -- rising clock edge
      -- zero pulses
      Tx_data_wr <= '0';
      rx_data_rd <= '0';
      register_data_out_valid <= '0';
      
      case current_state is
        when STATE_IDLE =>
          ---------------------------------------
          -- STATE_IDLE: wait for an incoming start
          if clk_enable_rx = '1' and rx_data_ready = '1' then
            if rx_data(7 downto 6) = hw_address then                       
              -- this is for us, output the register addr for the next state              
              register_address_buffer <= rx_data(3 downto 0);
              -- record if the master is reading or writing
              rw_bit <= rx_data(5);
              -- set this byte as read
              rx_data_rd <= '1';
              current_state <= STATE_START;              
            else
              -- this is not for us

              -- set this byte as read
              rx_data_rd <= '1';

              -- set the number of words we need to ignore
              if rx_data(5) = '1' then
                current_state <= STATE_IDLE;
              else
                current_state <= STATE_IGNORE;
                master_ignore <= '1';
              end if;
            end if;
          end if;
        when STATE_START =>
          ---------------------------------------
          -- STATE_START: check if the master is reading or writing
          if rw_bit = '1' then
            -- need another clock tick (state) to get the register data
            current_state <= STATE_READ_LOAD;
          else
            current_state <= STATE_WRITE_1;
          end if;
        when STATE_READ_LOAD =>
            -- The master is reading, so we need to go to the write address
            current_state <= STATE_READ_1;
            -- latch data at reg address
            local_data <= register_data_in;          
        when STATE_READ_1 =>
          ---------------------------------------
          -- STATE_READ_1: Write out bits 15 to 8 of the data
          if clk_enable_tx = '1' and tx_buffer_empty = '1' and tx_register_empty = '1' then
            Tx_data <= local_data(15 downto 8);
            tx_data_wr <= '1';              
            current_state <= STATE_READ_2;
          end if;
        when STATE_READ_2 =>
          ---------------------------------------
          -- STATE_READ_2: Write out bits 7 to 0 of the data
          if clk_enable_tx = '1' and tx_buffer_empty = '1' and  tx_register_empty = '1' then
            Tx_data <= local_data(7 downto 0);
            tx_data_wr <= '1';
            current_state <= STATE_END;
          end if;
        when STATE_WRITE_1 =>
          ---------------------------------------
          -- STATE_WRITE_1: read in bits 15 to 8 of the data
          if clk_enable_rx = '1' and Rx_data_ready = '1' then
            register_data_out(15 downto 8) <= rx_data;
            rx_data_rd <= '1';
            current_state <= STATE_WRITE_2;
          end if;
        when STATE_WRITE_2 =>
          ---------------------------------------
          -- STATE_WRITE_2: read in bits 7 to 0 of the data
          if clk_enable_rx = '1' and Rx_data_ready = '1' then
            register_data_out(7 downto 0) <= rx_data;
            rx_data_rd <= '1';
            register_data_out_valid <= '1';
            current_state <= STATE_END;
          end if;
        when STATE_END =>
          ---------------------------------------
          -- STATE_END: read in bits 7 to 0 of the data
          if  clk_enable_tx = '1' and  tx_buffer_empty = '1' and  tx_register_empty = '1' then
            tx_data <= sw_address & hw_address & register_address_buffer;
            tx_data_wr <= '1';
            current_state <= STATE_IDLE;
          end if;
        when STATE_IGNORE =>
          ---------------------------------------
          -- STATE_IGNORE: this isn't for us, but we need to read out the data
          -- goign to another slave
          if clk_enable_rx = '1' and rx_data_ready = '1' then
            -- ingore this data
            rx_data_rd <= '1';
            -- this takes two words from the master to move to STATE_IDLE
            if master_ignore = '1' then              
              master_ignore <= '0';
            else
              current_state <= STATE_IDLE;
            end if;            
          end if;          
        when others => null;
      end case;
    end if;
  end process slave_state_machine;
  
end architecture Behaviour;
