library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity SpillTiming is
  
  port (
    clk40   : in  std_logic;            -- 10 Mhz TDC clock
    reset   : in  std_logic;
    	
    reset_spill   : in  std_logic;      -- reset spill number
    reset_time    : in std_logic;       -- reset spill time

    busy          : in std_logic;             -- block any new spills
    spill         : in std_logic;       -- new spill
	 spill_incr    : in std_logic;       -- incr spill counter
    
    spill_number  : out std_logic_vector(23 downto 0);
    spill_time    : out std_logic_vector(43 downto 0)
    );
end SpillTiming;          
architecture Behavioral of SpillTiming is

  signal spill_number_counter : std_logic_vector(23 downto 0) := x"000001";
  signal spill_timer : std_logic_vector(43 downto 0) := x"00000000000";

begin  -- Behavioral


  timer: process (clk40, reset)
  begin  -- process timer
    if reset = '1' then          -- asynchronous reset (active high)
		spill_timer <= x"00000000000";
		spill_time <= x"00000000000";            
    elsif clk40'event and clk40 = '1' then  -- rising clock edge
		spill_timer <= std_logic_vector(unsigned(spill_timer) + 1);
		if reset_time = '1' then   
			spill_timer <= x"00000000000";      	
		elsif spill = '1' and busy = '0' then	
         --update the spill time
			spill_time <= spill_timer;			
		end if;
    end if;
  end process timer;
  
  spill_counter: process (clk40, reset)
  begin  -- process spill_counter
    if reset = '1' then           -- asynchronous reset (active low)
      spill_number_counter <=  x"000001";
		spill_number <= x"000000";		
    elsif clk40'event and clk40 = '1' then  -- rising clock edge	 		
		if reset_spill = '1' then
			spill_number <= x"000000"; -- update the output value
			spill_number_counter <=  x"000001";
		elsif (spill = '1' or spill_incr = '1') and busy = '0'  then	
			--update the output value
			spill_number <= spill_number_counter;  
			--set the next spill number
			spill_number_counter <= std_logic_vector(unsigned(spill_number_counter) + 1);        		
--		elsif spill_incr = '1' then
--			--set the next spill number
--			spill_number_counter <= std_logic_vector(unsigned(spill_number_counter) + 1);        		
		end if;
    end if;
  end process spill_counter;
	
  
end Behavioral;
  
