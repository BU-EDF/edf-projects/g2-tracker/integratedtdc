source vhdl/sim/i2c_helper.tcl
# source vhdl/sim/i2c_register.tcl

isim force add /i2c_register/reset 1
run 1 us

isim force add /i2c_register/i2c_address 1010101 -radix bin
isim force add /i2c_register/reset 0

isim force add /i2c_register/SDA 1
isim force add /i2c_register/SCL 1

run 1000 us

send_start /i2c_register/sda /i2c_register/scl
send_byte 10101010 /i2c_register/sda /i2c_register/scl
send_byte 00000000 /i2c_register/sda /i2c_register/scl
for { set i 0 } {$i < 8 } { incr i } {
    send_byte 00000000 /i2c_register/sda /i2c_register/scl
    send_byte 00000001 /i2c_register/sda /i2c_register/scl
    send_byte 00000010 /i2c_register/sda /i2c_register/scl
    send_byte 00000011 /i2c_register/sda /i2c_register/scl
}
send_stop /i2c_register/sda /i2c_register/scl


run 1000 us

send_start /i2c_register/sda /i2c_register/scl
send_byte 10101010 /i2c_register/sda /i2c_register/scl
send_byte 00000000 /i2c_register/sda /i2c_register/scl
send_restart /i2c_register/sda /i2c_register/scl
send_byte 10101011 /i2c_register/sda /i2c_register/scl
for { set i 0 } {$i < 32 } { incr i } {
    get_byte /i2c_register/sda /i2c_register/scl
}
send_stop /i2c_register/sda /i2c_register/scl


run 1000 us

