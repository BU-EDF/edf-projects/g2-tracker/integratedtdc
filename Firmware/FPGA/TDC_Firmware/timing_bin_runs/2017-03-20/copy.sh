#!/bin/bash
basename=$(basename $PWD)
cp ../../IntTDC-${basename}.hexout ./
scp ./IntTDC-${basename}.hexout dgastler@ohm.bu.edu:~/public_html/g-2/firmware
