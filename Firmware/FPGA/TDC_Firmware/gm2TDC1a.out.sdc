## Generated SDC file "gm2TDC1a.out.sdc"

## Copyright (C) 1991-2013 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

## DATE    "Tue Dec 17 18:24:38 2013"

##
## DEVICE  "EP3C5U256C6"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {C5_IN} -period 100.000 -waveform { 0.000 50.000 } [get_ports {C5_IN}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {inst7|altpll_component|auto_generated|pll1|clk[0]} -source [get_pins {inst7|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50.000 -multiply_by 40 -master_clock {C5_IN} [get_pins {inst7|altpll_component|auto_generated|pll1|clk[0]}] 
create_generated_clock -name {inst7|altpll_component|auto_generated|pll1|clk[1]} -source [get_pins {inst7|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50.000 -multiply_by 40 -phase 90.000 -master_clock {C5_IN} [get_pins {inst7|altpll_component|auto_generated|pll1|clk[1]}] 
create_generated_clock -name {inst7|altpll_component|auto_generated|pll1|clk[2]} -source [get_pins {inst7|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50.000 -multiply_by 20 -master_clock {C5_IN} [get_pins {inst7|altpll_component|auto_generated|pll1|clk[2]}] 
create_generated_clock -name {inst7|altpll_component|auto_generated|pll1|clk[3]} -source [get_pins {inst7|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50.000 -multiply_by 4 -master_clock {C5_IN} [get_pins {inst7|altpll_component|auto_generated|pll1|clk[3]}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -rise_to [get_clocks {C5_IN}] -setup 0.100  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -rise_to [get_clocks {C5_IN}] -hold 0.080  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -fall_to [get_clocks {C5_IN}] -setup 0.100  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -fall_to [get_clocks {C5_IN}] -hold 0.080  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -rise_to [get_clocks {C5_IN}] -setup 0.100  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -rise_to [get_clocks {C5_IN}] -hold 0.080  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -fall_to [get_clocks {C5_IN}] -setup 0.100  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -fall_to [get_clocks {C5_IN}] -hold 0.080  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -setup 0.080  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -setup 0.080  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -setup 0.080  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -setup 0.080  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -setup 0.080  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -setup 0.080  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -rise_to [get_clocks {C5_IN}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -fall_to [get_clocks {C5_IN}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.080  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.080  
set_clock_uncertainty -rise_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -setup 0.080  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -setup 0.080  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -setup 0.080  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -setup 0.080  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -setup 0.080  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -setup 0.080  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -rise_to [get_clocks {C5_IN}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -fall_to [get_clocks {C5_IN}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.080  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.080  
set_clock_uncertainty -fall_from [get_clocks {C5_IN}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[2]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  


#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

