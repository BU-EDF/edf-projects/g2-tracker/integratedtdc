#define buffsize  128

//THE PINS MUST BE THE ONES ON THE MICROCONTROLLER (IN () ON THE SCHEMATIC)
#define mask B11110111 
//#define momask B
//#define mimask B
#define datapin B00001000 //DATA is out on pin 3
#define clockpin B00100000 //DCLK is out on pin 5
//#define serialclock B00100000 //clock for serial is pin 13
//#define MOSI B00001000 //flash data is out on pin 11
//#define MISO B00010000 //data from flash is in on pin 12
#define slavepin B00000100 //slave select is pin 10
//#define configON B00000010 //nConfig is on pin 1
//#define configOFF B00000000 //
//#define INIT_DONE B00001000 //init_done is pin 4
//#define CONF_DONE B00100000 //CONF_DONE is on pin 2

#include <SPI.h>
#include <ctype.h>

//Pin numbers need to be changed

int DATA = 3;
int DCLK = 5;
//int nCONFIG = 1;
int INIT_DONE = 4;
int CONF_DONE = 2;
//int send_check = 0;
uint32_t bytes_read = 0;
uint8_t data[100] = {0};
uint8_t flash_to_fpga[1] = {0};

uint8_t command[32] = {0}; 
uint8_t positionbuff[32] = {0};
uint8_t sizebuff[32] = {0};
uint8_t flashbuff[buffsize] = {0};
uint32_t flash_address = {0};
int flash_data_pin = 11;

const int slaveSelectPin = 10;

int key = 0;

void setup(){

pinMode(flash_data_pin, OUTPUT); 
pinMode (slaveSelectPin, OUTPUT); // set the slaveSelectPin as an output:
//pinMode(nCONFIG, OUTPUT);
pinMode(DCLK, OUTPUT);
pinMode(DATA, OUTPUT);
pinMode(INIT_DONE, INPUT);
pinMode(CONF_DONE,OUTPUT);

for(int z = 0; z<buffsize; z++){
  flashbuff[z] = 0xff;
}  

// initialize SPI:
SPI.begin(); 
  
Serial.begin(115200);

//Serial.print(">");
}



void loop(){
  //create a buffer  
  /*if(digitalRead(CONF_DONE) != 1){
    run_fpga_conf();
  }else*/ if (Serial.available() > 0){
    command[key] = Serial.read();    
    if((command[key] == 0xA) || // LF
       (command[key] == 0xD)){  // CR
    //newline things
      Serial.println();
      initializefct();
      tokenize();    
      run_command();
      Serial.print(">"); 
      uint8_t command[] = {0}; 
      key = 0;     
    }else if(command[key] == 0x8){
      backspacefct();
    }else if ((command[key] >= 32) || // space
             (command[key] <0X7F)){  // delete
      Serial.write(command[key]);
      key++;
      if(key == 32){ 
        Serial.println("  buffer overflow");
        Serial.print(">");
        uint8_t command = {0}; 
        key = 0;
      }
    }
  }
}

void initializefct(){
  for(int z = 0; z<32;z++){
    positionbuff[z] = 0;
    sizebuff[z] = 0;
  }
  return; 
}

void backspacefct(){
  if (key>0){
    Serial.write(0x8);
    Serial.write(' ');
    Serial.write(0x8);
    key--;
  }
  return;
}

void tokenize(){
  int location = 0;
  positionbuff[location] = 0;
  if (command[key-1] != 0x20){  
    command[key] = 0x20; // pretend that a space was entered last
  }  
  for(int z = 0; z<key+1; z++){
    if (command[z] == 0x20){
      command[z] = 0x0; // null terminate each tokenized string
      location++;
      positionbuff[location] = z+1;
      sizebuff[location-1] = positionbuff[location]-positionbuff[location-1]-1;
    }    
    else if(z==(positionbuff[location]+8)){
      for(int x = key+1; x>z; x--){
        command[x] = command[x-1];
      }
      key+=1;
      command[z] = 0x0; // null terminate each tokenized string
      location++;
      positionbuff[location] = z+1;
      sizebuff[location-1] = positionbuff[location]-positionbuff[location-1]-1;
      //positionbuff[location]-=1;    
    } 
  }
  return;  
}

uint8_t to_lower(uint8_t letter){
  if(letter<91 && letter>64){ 
    return letter+0x20;
  }
  else{
    return letter;
  }
}

int string_size(const char* cmd){
  int strlength = 0; 
  for(int n=0; n<100; n++){
    if (cmd[n] !=0){
      strlength++;
    }
    else{
      return strlength; 
    }
  } 
}

int command_cmp(const char* cmd){
  uint8_t max_size = sizebuff[0];
  //Serial.print(string_size(cmd));
  //Serial.print(" ");
  //Serial.println(max_size);  
  if (max_size != string_size(cmd)){
    return 0;
  }
  if (max_size ==0){
    return 0;
  }
  for(int i = 0; i<max_size; i++){
    if (to_lower(command[i]) != cmd[i]){
      return 0;      
    }
  }  
  return 1;
}

uint8_t statusreg(){
  digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0x05);
  uint8_t data1 = SPI.transfer(0x00);
  digitalWrite(slaveSelectPin,HIGH);
  
  return data1;
}

void bad_command(){
  Serial.println("Bad Command");
  return;
} 

void run_command(){
  if (command_cmp("echo")){ //Echo
    run_echo();
  }else if(command_cmp("fi")){ //Flash info
    run_fi();
  }else if(command_cmp("fe")){ //Flash erase
    run_fe();
  }else if(command_cmp("fw")){ //Flash write
    run_fw();
  }else if(command_cmp("fc")){ //Flash clear
    run_fc();
  }else if(command_cmp("fww")){ //Flash write word
    run_fww();
  }else if(command_cmp("fwr")){ //Flash word read
    run_fwr();
  }else if(command_cmp("fr")){ //Flash read
    run_fr();
  }else if(command_cmp("faw")){ //Flash address write
    run_faw();
  }else if(command_cmp("fsw")){ //Flash status write
    run_fsw();
  }else if(command_cmp("far")){ //Flash address read
    run_far();  
  }else if(command_cmp("fsr")){ //Flash status read
    run_fsr();
  }else if(command_cmp("fpga")){ //reconfigure fpga
    run_fpga_conf();  
  }else{ 
    bad_command();
  }
}

void run_echo(){
  Serial.println("ECHO:");  
  for (int z = 5; z<key; z++){
    if(command[z] == 0x0){
      Serial.write(0x20);  
    }
    else{
      Serial.write(command[z]);
    }
  }
  Serial.println();        
}

void run_fi(){
//  Serial.println("fi: ");
  //gets info about the flash
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  Serial.println("Buffer size: 128 bytes");   
  digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0x9F);
  uint8_t data1 = SPI.transfer(0x00);
  uint8_t data2 = SPI.transfer(0x00);
  uint8_t data3 = SPI.transfer(0x00);
  digitalWrite(slaveSelectPin,HIGH);
  if((data1 == 0x20) && (data2 == 0x20) && (data3 == 0x16)){
    Serial.println("info is correct");
  }
  else{
    Serial.println("info is incorrect");
  }      
}

void run_fe(){
//  Serial.println("fe:");   
  //erases the flash for writing  
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0x06);    
  digitalWrite(slaveSelectPin,HIGH); 
  /*while (statusreg() != 2){
  //Serial.println(statusreg());
    delay(500);  
    digitalWrite(slaveSelectPin,LOW);
    SPI.transfer(0x06);    
    digitalWrite(slaveSelectPin,HIGH); 
    delay(500);      
  }*/
  digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0xC7);
  digitalWrite(slaveSelectPin,HIGH);  
  
  while (statusreg()&0x01);  //delay until bit 0 of the status register is 0 
}

void run_fw(){
//  Serial.println("fw:");
  //write big buffer of words into flash memory  
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0x06);    
  digitalWrite(slaveSelectPin,HIGH); 
  /*while (statusreg() != 2){
    //Serial.println(statusreg());
    delay(500);  
    digitalWrite(slaveSelectPin,LOW);
    SPI.transfer(0x06);    
    digitalWrite(slaveSelectPin,HIGH); 
    delay(500);      
  }*/
  digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0x02);
  SPI.transfer((flash_address>>16)&0xff);
  SPI.transfer((flash_address>>8)&0xff);
  SPI.transfer((flash_address)&0xff);
  for(int z = 0; z<buffsize; z++){
    SPI.transfer(flashbuff[z]);
  }
  digitalWrite(slaveSelectPin,HIGH);
  flash_address+=buffsize;  
  
  while (statusreg()&0x01);  //delay until bit 0 of the status register is 0   
}

void run_fc(){
//  Serial.println("flash buffer cleared");     
  //clear large flash buffer
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  for(int z = 0; z<buffsize; z++){
    flashbuff[z] = 0xff;
  }  
}

void run_fww(){
//  Serial.println("fww:");
  strtol("0x7", NULL, 0);
  //write the given word to a big buffer to be sent to the flash
  if(isxdigit(command[4]) && (sizebuff[1] == 1) && (sizebuff[2] == 8) && (sizebuff[3] == 8)){
    for(int z=0; z<4; z++){
      flashbuff[z+(strtoul(((char*)command+4),NULL,16))*8] = (strtoul((char*)command+positionbuff[2],NULL,16)>>(z*8)) & 0xFF;      
    }
    for(int z=4; z<8; z++){
      flashbuff[z+(strtoul((((char*)command+4)),NULL,16))*8] = (strtoul((char*)command+positionbuff[3],NULL,16)>>((z-4)*8)) & 0xFF;          
    }
  }
  else{
    Serial.println("improper size");
    return;  
  }   
}

void run_fwr(){
  Serial.println("fwr:");
  //read that given written word
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }      
  for(int y = 0; y<(buffsize/8);y++){
    for(int z = 0; z<8; z++){
      Serial.print(flashbuff[(y*8)+z], HEX);
      Serial.print(' ');
    }
    Serial.println(); 
  }  
}

void run_fr(){
  Serial.println("fr:");
  //read the flash at the address into the 128 byte buffer on the uC (address is auto incremented)    
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }     
  digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0x03);
  SPI.transfer((flash_address>>16)&0xff);
  SPI.transfer((flash_address>>8)&0xff);
  SPI.transfer((flash_address)&0xff);
  for(int z = 0; z<buffsize; z++){
    Serial.println(SPI.transfer(0x00), HEX);
  }
  digitalWrite(slaveSelectPin,HIGH);
  flash_address+=buffsize;  
  
  while (statusreg()&0x01);  //delay until bit 0 of the status register is 0   
}

void run_faw(){
  Serial.println("faw:");    
  //accept the address  
  for(int z = 4; z<10; z++){
    if(!isxdigit(command[z])){
      Serial.println("improper address");
      flash_address = 0;
      return; 
    }
  }
    flash_address = (strtoul(((char*)command+4),NULL,16)); 
  Serial.println("address:");
  Serial.println(flash_address, HEX);   
}

void run_fsw(){
  Serial.println("fsw:");  
  //write to the status register for the flash      
  //while (statusreg() != 2){
  //Serial.println(statusreg()); 
  digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0x06);    
  digitalWrite(slaveSelectPin,HIGH);
  delay(1000);    
  //}
  digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0x01);
  if(sizebuff[1] != 2){
    Serial.println("improper size");
    return;  
  }  
  SPI.transfer(strtoul(((char*)command+4),NULL,16));    
  //SPI.transfer(0x00);
  digitalWrite(slaveSelectPin,HIGH);   
  
  while (statusreg()&0x01);  //delay until bit 0 of the status register is 0   
}

void run_far(){
  Serial.println("far:");
  //print back the last adress that was given    
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  Serial.println(flash_address, HEX);  
}

void run_fsr(){
  Serial.println("fsr:");    
  //read the status register of the flash   
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0x05);
  uint8_t data1 = SPI.transfer(0x00);
  Serial.println(data1, DEC);
  digitalWrite(slaveSelectPin,HIGH);
  
  while (statusreg()&0x01);  //delay until bit 0 of the status register is 0   
}

//only 2 bits of 0xaa are being clocked out

void run_fpga_conf(){
  Serial.println("fpga_conf: ");
  //reconfigure the fpga    
  flash_address = 0;

  Serial.end();
  
  //PORTD = configON; //digitalWrite(nCONFIG, 1);
  delay(5);
  //PORTD = configOFF; //digitalWrite(nCONFIG, 0);
  delay(5);
 
  PORTB = 0;
  //PORTD ^= clockpin;//digitalWrite(DCLK, 0);   
  //PORTD ^= clockpin;//digitalWrite(DCLK, 1);    
  //PORTD = configON;//digitalWrite(nCONFIG, 1);
  bytes_read = 0; 
  SPI.transfer(0x03);
  SPI.transfer((flash_address>>16)&0xff);
  SPI.transfer((flash_address>>8)&0xff);
  SPI.transfer((flash_address)&0xff); 
  while(1/*!(PIND&0x10)*/){ //PIND != B01010000 //while CONF_DONE and INIT_DONE aren't true 
    while (1/*digitalRead(CONF_DONE) != 1*/) { //while CONF DONE isn't true 
      PORTB &= ~slavepin; //digitalWrite(slaveSelectPin,LOW);      
      //SPI.begin();
      flash_to_fpga[0]= SPI.transfer(0x00); 
      //Serial.print(flash_to_fpga[0], HEX);
      PORTD |= datapin; //digitalWrite(DATA,HIGH);
      //SPI.end();
      flash_address++; 
      send_bytes(flash_to_fpga, 1);
      bytes_read++;
      if(bytes_read >= 25){
        break;
      }  
    }  
    PORTD ^= clockpin;//digitalWrite(DCLK, 0);
    PORTD ^= clockpin;//digitalWrite(DCLK, 1);
    PORTD &= ~datapin;//digitalWrite(DATA, 0)
    //PORTD |= B00000100;//digitalWrite(CONF_DONE,HIGH); //I can't actually change this myself   
    if(bytes_read >= 25){
      break;      PORTD ^= clockpin;//digitalWrite(DCLK, 0);
      PORTD ^= clockpin;//digitalWrite(DCLK, 1);
    }  
}  
  flash_address = 0;
  //delay(1000);
  Serial.begin(115200);
  //SPI.begin();
  return;
  while (statusreg()&0x01);  //delay until bit 0 of the status register is 0   
}
  
int send_bytes(uint8_t *data, int length){
  uint8_t *data_byte = data;
  uint8_t *data_end = data+length;
  while(data_byte < data_end){
    uint8_t local_data = *data_byte;
    for(int bits = 0; bits< 8; bits++){
      PORTD ^= clockpin;//digitalWrite(DCLK, 0);
    
      if(local_data & 1){
        PORTD |= datapin;//digitalWrite(data_pin, (local_data & 1));
      }
      else{
        PORTD &= mask; 
      }
      PORTD ^= clockpin;//digitalWrite(DCLK, 1);  
      local_data >>= 1;
    }
    data_byte++;
  }  
}

/*
int send_serial(uint8_t data){
  uint8_t *data_byte = data;
  uint8_t *data_end = data+1;
  while(data_byte < data_end){
    uint8_t local_data = *data_byte;
    for(int bits = 0; bits< 8; bits++){
      if(local_data & 1){
        PORTD |= MOSI;//digitalWrite(data_pin, (local_data & 1));
      }
      else{
        PORTD &= momask; 
      }
      PORTB ^= serialclock;//digitalWrite(DCLK, 0);
      PORTB ^= serialclock;//digitalWrite(DCLK, 1);
      local_data >>= 1;
    }
    data_byte++;
  }  
}

int get_serial(){
  
}



*/
