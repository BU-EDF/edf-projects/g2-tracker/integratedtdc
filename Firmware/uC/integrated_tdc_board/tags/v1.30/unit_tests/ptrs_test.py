#!/usr/bin/env python

import serial
import argparse
import difflib
import sys
import re


command_num = 0

#array of ptrs commands
input_string = []

#array of expected outputs
expected_output = []



def WriteCommand(ser,line):
   if line.find("\n") != -1:
       raise cmd("new line in command")
   # write command and make sure the reponse makes sense                                                                                                                                                                                                                          
   for char in line:
           ser.write(char)
           rd = ser.read()
           if rd != char:
                   print "bad char! " + str(rd) + " is not expected " + str(char)
                   raise cmd("bad char")
#   ser.write(line)                                                                                                                                                                                                                                                               
#execute the command and read the output                                                                                                                                                                                                                                          
   ser.write("\n")
   rd = ser.read()
   #print "\"", rd, "\" :", hex(ord(rd))
   while rd != "\n":
       rd = ser.read()
       #print "\"", rd, "\" :", hex(ord(rd))
   rd = ser.read()
   #print "\"", rd, "\" :", hex(ord(rd))
   line = ""
   while rd != ">":
       line = line + rd
       rd = ser.read()
#    line = line.split()                                                                                                                                                                                                                                                          
   return line

def add_command(new_input, new_output):
    global command_num
    command_num += 1
    input_string.append(new_input)
    expected_output.append(new_output)

def compare_input_output():
    for x in range(0,command_num): 
        ptrs_command =  WriteCommand(ser, input_string[x])
        
        ptrs_command_replaced = ptrs_command.replace("\n","")
        ptrs_command_replaced = ptrs_command_replaced.replace("\r","")
    
        output_replaced = expected_output[x]
        output_print = expected_output[x]
        
        output_replaced = output_replaced.replace("\n","")
        output_replaced = output_replaced.replace("\r","")
        
        print "testing: \"" + input_string[x] + "\""
        if ptrs_command_replaced == output_replaced:
            print "pass"
            print ""
        else:
            print "fail"
            print ""
            lines1 = ptrs_command.splitlines(1)
            lines2 = output_print.splitlines(1)
            diff = difflib.ndiff(ptrs_command.splitlines(1),output_print.splitlines(1))
            #diff =  difflib.ndiff(lines1[i],lines2[i])
            print "     ".join(diff),
            #print lines1[i]
            #print ptrs_command
            #print output_print

# Inputs ******************************************************************************************************

parser = argparse.ArgumentParser(description='Program flash from intel hex file via serial. Default to 250000 baud rate on ttyUSB0.')

parser.add_argument('-b','--baud', help='Set baudrate. Default is 250000.', type=int, default=250000)
parser.add_argument('-D','--Device', help='Change USB port. Default is /dev/ttyUSB0', default="/dev/ttyUSB0")
args = vars(parser.parse_args())

baud_rate = args["baud"]
device = args["Device"]
# Main Function **********************************************************************************************

#open the serial port
# ser = serial.Serial("/dev/ttyUSB0",250000);
ser = serial.Serial(device, baud_rate);

WriteCommand(ser, "")

#print ""
#print "writing ptrs command"
#print ""

add_command("ptrs aaaa aaaa","01CB   ptrs~aaaa~aaaa~\n\r00   04   01D0   aaaa\n\r01   04   01D5   aaaa\n\r")
add_command("ptrs aa aaaa aa aaaa","01CB   ptrs~aa~aaaa~aa~aaaa~\n\r00   02   01D0   aa\n\r01   04   01D3   aaaa\n\r02   02   01D8   aa\n\r03   04   01DB   aaaa\n\r")
add_command("ptrs 0123456789abcdef","01CB   ptrs~01234567~89abcdef~\n\r00   08   01D0   01234567\n\r01   07   01D9   89abcdef\n\r")
add_command("ptrs 000000", "01E5   ptrs~000000~n\r00   06   01EA   000000\n\r")

compare_input_output()

        


