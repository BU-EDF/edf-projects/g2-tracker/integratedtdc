/*-------------------------------------------------------------------------
  Arduino library to control a wide variety of WS2811- and WS2812-based RGB
  LED devices such as Adafruit FLORA RGB Smart Pixels and NeoPixel strips.
  Currently handles 400 and 800 KHz bitstreams on 8, 12 and 16 MHz ATmega
  MCUs, with LEDs wired for various color orders.  8 MHz MCUs provide
  output on PORTB and PORTD, while 16 MHz chips can handle most output pins
  (possible exception with upper PORT registers on the Arduino Mega).

  Written by Phil Burgess / Paint Your Dragon for Adafruit Industries,
  contributions by PJRC, Michael Miller and other members of the open
  source community.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing products
  from Adafruit!

  -------------------------------------------------------------------------
  This file is part of the Adafruit NeoPixel library.

  NeoPixel is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of
  the License, or (at your option) any later version.

  NeoPixel is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with NeoPixel.  If not, see
  <http://www.gnu.org/licenses/>.
  -------------------------------------------------------------------------*/

#include "NeoPixel.h"
#include <stdint.h> //uint8/16_t
#include <string.h> //memset
#include <avr/io.h> //PORTS
#include <avr/interrupt.h> //interrupts


static uint8_t pixels[PIXEL_COUNT*PIXEL_BYTE]; // Holds LED color values (3 bytes each)
static const uint8_t rOffset = 1;       // Index of red byte within each 3-byte pixel
static const uint8_t gOffset = 0;       // Index of green byte
static const uint8_t bOffset = 2;       // Index of blue byte

volatile uint8_t *port = &PIXEL_PORT;         // Output PORT register
static uint8_t pinMask = (1<< PIXEL_PIN);         // Output PORT bitmask
static uint32_t endTime;       // Latch timing reference

void NeoPixel_show(void) {
  // Data latch = 50+ microsecond pause in the output stream.  Rather than
  // put a delay at the end of the function, the ending time is noted and
  // the function will simply hold off (if needed) on issuing the
  // subsequent round of data until the latch time has elapsed.  This
  // allows the mainline code to start generating the next frame of data
  // rather than stalling for the latch.
  while(!NeoPixel_canShow());
  // endTime is a private member (rather than global var) so that mutliple
  // instances on different pins can be quickly issued in succession (each
  // instance doesn't delay the next).

  // In order to make this code runtime-configurable to work with any pin,
  // SBI/CBI instructions are eschewed in favor of full PORT writes via the
  // OUT or ST instructions.  It relies on two facts: that peripheral
  // functions (such as PWM) take precedence on output pins, so our PORT-
  // wide writes won't interfere, and that interrupts are globally disabled
  // while data is being issued to the LEDs, so no other code will be
  // accessing the PORT.  The code takes an initial 'snapshot' of the PORT
  // state, computes 'pin high' and 'pin low' values, and writes these back
  // to the PORT register as needed.

  cli(); // Need 100% focus on instruction timing

  volatile uint16_t i   = PIXEL_COUNT*PIXEL_BYTE; // Loop counter
  volatile uint8_t  *ptr = pixels;   // Pointer to next byte
  volatile uint8_t b   = *ptr++;   // Current byte value
  volatile uint8_t hi;             // PORT w/output bit set high
  volatile uint8_t lo;             // PORT w/output bit set low

  // Hand-tuned assembly code issues data to the LED drivers at a specific
  // rate.  There's separate code for different CPU speeds (8, 12, 16 MHz)
  // for both the WS2811 (400 KHz) and WS2812 (800 KHz) drivers.  The
  // datastream timing for the LED drivers allows a little wiggle room each
  // way (listed in the datasheets), so the conditions for compiling each
  // case are set up for a range of frequencies rather than just the exact
  // 8, 12 or 16 MHz values, permitting use with some close-but-not-spot-on
  // devices (e.g. 16.5 MHz DigiSpark).  The ranges were arrived at based
  // on the datasheet figures and have not been extensively tested outside
  // the canonical 8/12/16 MHz speeds; there's no guarantee these will work
  // close to the extremes (or possibly they could be pushed further).
  // Keep in mind only one CPU speed case actually gets compiled; the
  // resulting program isn't as massive as it might look from source here.

  // 8 MHz(ish) AVR ---------------------------------------------------------


  volatile uint8_t n1, n2 = 0;  // First, next bits out

  // Squeezing an 800 KHz stream out of an 8 MHz chip requires code
  // specific to each PORT register.  At present this is only written
  // to work with pins on PORTD or PORTB, the most likely use case --
  // this covers all the pins on the Adafruit Flora and the bulk of
  // digital pins on the Arduino Pro 8 MHz (keep in mind, this code
  // doesn't even get compiled for 16 MHz boards like the Uno, Mega,
  // Leonardo, etc., so don't bother extending this out of hand).
  // Additional PORTs could be added if you really need them, just
  // duplicate the else and loop and change the PORT.  Each add'l
  // PORT will require about 150(ish) bytes of program space.
  
  // 10 instruction clocks per bit: HHxxxxxLLL
  // OUT instructions:              ^ ^    ^   (T=0,2,7)
  
  
  //Because of the inverter on the pin to drive the LEDs, lo is high and hi is low.
  lo = *port |  pinMask;
  hi = *port & ~pinMask;
  
  n1 = lo;
  if(b & 0x80) n1 = hi;
  
  // Dirty trick: RJMPs proceeding to the next instruction are used
  // to delay two clock cycles in one instruction word (rather than
  // using two NOPs).  This was necessary in order to squeeze the
  // loop down to exactly 64 words -- the maximum possible for a
  // relative branch.
  
  //PORTC = lo;
  //PORTC = hi;
  //PORTC = lo;
  asm volatile(
	       "headD:"                   "\n\t" // Clk  Pseudocode
	       // Bit 7:
"out  %[port] , %[hi]"    "\n\t" // 1    PORT = hi
	       "mov  %[n2]   , %[lo]"    "\n\t" // 1    n2   = lo
	       "out  %[port] , %[n1]"    "\n\t" // 1    PORT = n1
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       "sbrc %[byte] , 6"        "\n\t" // 1-2  if(b & 0x40)
	       "mov %[n2]   , %[hi]"    "\n\t" // 0-1   n2 = hi
	       "out  %[port] , %[lo]"    "\n\t" // 1    PORT = lo
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       // Bit 6:
	       "out  %[port] , %[hi]"    "\n\t" // 1    PORT = hi
	       "mov  %[n1]   , %[lo]"    "\n\t" // 1    n1   = lo
	       "out  %[port] , %[n2]"    "\n\t" // 1    PORT = n2
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       "sbrc %[byte] , 5"        "\n\t" // 1-2  if(b & 0x20)
	       "mov %[n1]   , %[hi]"    "\n\t" // 0-1   n1 = hi
	       "out  %[port] , %[lo]"    "\n\t" // 1    PORT = lo
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       // Bit 5:
	       "out  %[port] , %[hi]"    "\n\t" // 1    PORT = hi
	       "mov  %[n2]   , %[lo]"    "\n\t" // 1    n2   = lo
	       "out  %[port] , %[n1]"    "\n\t" // 1    PORT = n1
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       "sbrc %[byte] , 4"        "\n\t" // 1-2  if(b & 0x10)
	       "mov %[n2]   , %[hi]"    "\n\t" // 0-1   n2 = hi
	       "out  %[port] , %[lo]"    "\n\t" // 1    PORT = lo
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       // Bit 4:
	       "out  %[port] , %[hi]"    "\n\t" // 1    PORT = hi
	       "mov  %[n1]   , %[lo]"    "\n\t" // 1    n1   = lo
	       "out  %[port] , %[n2]"    "\n\t" // 1    PORT = n2
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       "sbrc %[byte] , 3"        "\n\t" // 1-2  if(b & 0x08)
	       "mov %[n1]   , %[hi]"    "\n\t" // 0-1   n1 = hi
	       "out  %[port] , %[lo]"    "\n\t" // 1    PORT = lo
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       // Bit 3:
	       "out  %[port] , %[hi]"    "\n\t" // 1    PORT = hi
	       "mov  %[n2]   , %[lo]"    "\n\t" // 1    n2   = lo
	       "out  %[port] , %[n1]"    "\n\t" // 1    PORT = n1
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       "sbrc %[byte] , 2"        "\n\t" // 1-2  if(b & 0x04)
	       "mov %[n2]   , %[hi]"    "\n\t" // 0-1   n2 = hi
	       "out  %[port] , %[lo]"    "\n\t" // 1    PORT = lo
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       // Bit 2:
	       "out  %[port] , %[hi]"    "\n\t" // 1    PORT = hi
	       "mov  %[n1]   , %[lo]"    "\n\t" // 1    n1   = lo
	       "out  %[port] , %[n2]"    "\n\t" // 1    PORT = n2
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       "sbrc %[byte] , 1"        "\n\t" // 1-2  if(b & 0x02)
	       "mov %[n1]   , %[hi]"    "\n\t" // 0-1   n1 = hi
	       "out  %[port] , %[lo]"    "\n\t" // 1    PORT = lo
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       // Bit 1:
	       "out  %[port] , %[hi]"    "\n\t" // 1    PORT = hi
	       "mov  %[n2]   , %[lo]"    "\n\t" // 1    n2   = lo
	       "out  %[port] , %[n1]"    "\n\t" // 1    PORT = n1
	       "rjmp .+0"                "\n\t" // 2    nop nop
	       "sbrc %[byte] , 0"        "\n\t" // 1-2  if(b & 0x01)
	       "mov %[n2]   , %[hi]"    "\n\t" // 0-1   n2 = hi
	       "out  %[port] , %[lo]"    "\n\t" // 1    PORT = lo
	       "sbiw %[count], 1"        "\n\t" // 2    i-- (don't act on Z flag yet)
	       // Bit 0:
	       "out  %[port] , %[hi]"    "\n\t" // 1    PORT = hi
	       "mov  %[n1]   , %[lo]"    "\n\t" // 1    n1   = lo
	       "out  %[port] , %[n2]"    "\n\t" // 1    PORT = n2
	       "ld   %[byte] , %a[ptr]+" "\n\t" // 2    b = *ptr++
	       "sbrc %[byte] , 7"        "\n\t" // 1-2  if(b & 0x80)
	       "mov %[n1]   , %[hi]"    "\n\t" // 0-1   n1 = hi
	       "out  %[port] , %[lo]"    "\n\t" // 1    PORT = lo
	       "brne headD"              "\n"   // 2    while(i) (Z flag set above)
	       : [byte]  "+r" (b),
		 [n1]    "+r" (n1),
		 [n2]    "+r" (n2),
		 [count] "+w" (i)
	       : [port]   "I" (_SFR_IO_ADDR(PIXEL_PORT)),
		 [ptr]    "e" (ptr),
		 [hi]     "r" (hi),
		 [lo]     "r" (lo)
	       );  
  sei();
  //endTime = micros(); // Save EOD time for latch on next call
}


// Set pixel color from separate R,G,B components:
void NeoPixel_setPixelColor(
 uint16_t n, uint8_t r, uint8_t g, uint8_t b) {

  if(n < PIXEL_COUNT) {
    uint8_t *p = &pixels[n * 3];    // 3 bytes per pixel
    p[rOffset] = r;          // R,G,B always stored
    p[gOffset] = g;
    p[bOffset] = b;
  }
}

// Returns pointer to pixels[] array.  Pixel data is stored in device-
// native format and is not translated here.  Application will need to be
// aware of specific pixel data format and handle colors appropriately.
uint8_t *NeoPixel_getPixels(void) {
  return pixels;
}

void NeoPixel_clear() {
  memset(pixels, 0, PIXEL_COUNT*PIXEL_BYTE);
}
