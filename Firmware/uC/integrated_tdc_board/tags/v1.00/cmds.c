#include <stdlib.h>
#include <ctype.h>
#include "utils.h"
#include "cmds.h"
#include "SPI_cmds.h"
#include "UART_funcs.h"

#define flash_buff_size 128 //number of bytes to be writen to and read from the flash per read/write 
#define flash_to_fpga_size 1

uint8_t flashbuff[flash_buff_size];
uint32_t flash_address = 0;

uint8_t program_interrupt = 0;

//------------------------------------------------------------------------------
void cmd_echo(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //Serial.println("ECHO:");  
  for (uint8_t position = 0;(position<MAX_ARGS)&&(arg_size[position] !=0); position++){
    UART_print_noline(args[position]);
    // if (args[position][1] != 0x00 || 0x20){
    UART_print_noline(" ");
    //}
  }
  UART_print("");        
}

//------------------------------------------------------------------------------
void cmd_fi(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //Serial.println("fi: ");
  if(arg_size[0] != 0){
    UART_print("improper size");
    return;  
  }   
  UART_print("Buffer size: 128 bytes");   
  PORTB &= ~(1<<2); //set chip select pin low
  SPI_transfer_blocking(0x9F);
  uint8_t data1 = SPI_transfer_blocking(0x00);
  uint8_t data2 = SPI_transfer_blocking(0x00);
  uint8_t data3 = SPI_transfer_blocking(0x00);
  PORTB |= (1<<2); //set chip select pin high
  if((data1 == 0x20) && (data2 == 0x20) && (data3 == 0x16)){
    UART_print("info is correct");
  }
  else{
    UART_print("info is incorrect");
  }   
}

//------------------------------------------------------------------------------
void cmd_fe(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //Serial.println("fe:");   
  if(arg_size[0] != 0){
    UART_print("improper size");
    return;  
  }   
  PORTB &= ~(1<<2); //set chip select pin low
  SPI_transfer_blocking(0x06);    
  PORTB |= (1<<2); //set chip select pin high

  PORTB &= ~(1<<2); //set chip select pin low
  SPI_transfer_blocking(0xC7);
  PORTB |= (1<<2); //set chip select pin high
  
  check_WIP();
}

//------------------------------------------------------------------------------
void cmd_fw(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //  Serial.println("fw:");
  if(arg_size[0] != 0){
    UART_print("improper size");
    return;  
  }   
  PORTB &= ~(1<<2); //set chip select pin low
  SPI_transfer_blocking(0x06);    
  PORTB |= (1<<2); //set chip select pin high
  PORTB &= ~(1<<2); //set chip select pin low
  SPI_transfer_blocking(0x02); //send the command to program the flash
  SPI_transfer_blocking((flash_address>>16)&0xff); //send to the flash the address to be read from
  SPI_transfer_blocking((flash_address>>8)&0xff);
  SPI_transfer_blocking((flash_address)&0xff);
  for(uint8_t iByte = 0; iByte<flash_buff_size; iByte++){ //write to the flash
    SPI_transfer_blocking(flashbuff[iByte]);
  }
  PORTB |= (1<<2); //set chip select pin high
  flash_address+=flash_buff_size; //increment the address
  
  check_WIP; 
}

//------------------------------------------------------------------------------
void cmd_fc(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //Serial.println("flash buffer cleared");     
  if(arg_size[0] != 0){
    UART_print("improper size");
    return;  
  }   
  for(int iByte = 0; iByte<flash_buff_size; iByte++){
    flashbuff[iByte] = 0xff;
  }  
}

//------------------------------------------------------------------------------
void cmd_fww(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //  Serial.println("fww:");
  if( (arg_size[0] == 1)   && // argument 1 is one character long
      //isxdigit(args[0][0]) && // arguments 1 is a hex digit
      (arg_size[1] == 8)   && // argument 2 is 8 chars long (1 32bit word)
      (arg_size[2] == 8) ){   // argument 3 is 8 chars long
    uint8_t offset = strtoul((char*)&(args[0][0]),NULL,16)*8;  //offset in memory for this 64bit word
    uint32_t low_word = strtoul((args[2]),NULL,16);
    uint32_t high_word = strtoul((args[1]),NULL,16);
        
    uint32_t low_word_bits = low_word;
    uint32_t high_word_bits = high_word;
   
    // UART_print_hex(low_word_bits); //print low_word as hex
    
    // UART_print("");
    
    // UART_print_hex(high_word_bits); //print high_word as hex
        
    // UART_print("");
    for(int iByte=0; iByte<8; iByte++){      
      if (iByte < 4){
        flashbuff[offset + iByte] = (low_word >> iByte*8)&0xFF;
      }else{
        flashbuff[offset + iByte] = (high_word >> (iByte-4)*8)&0xFF;
	//UART_print_hex(flashbuff[offset+iByte]); //should print as hex
	//UART_print("");
      }
    }
  }
  else{
    UART_print("improper size");
    return;  
  }   
}

//------------------------------------------------------------------------------
void cmd_fwr(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //Serial.println("fwr:");
  if(arg_size[0] != 0){
    UART_print("improper size");
    return;  
  }      

  uint64_t * flashbuff_32 = (uint64_t*)flashbuff;

  for(uint8_t iWord = 0; iWord<(sizeof(flashbuff)/(sizeof(uint64_t)));iWord++){
    UART_print_hex(flashbuff_32[iWord]);
    // UART_print_hex(y);
    UART_print("");
  }  
}

//------------------------------------------------------------------------------
void cmd_fr(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //Serial.println("fr:");   
  if(arg_size[0] != 0){
    UART_print("improper size");
    return;  
  }     
  PORTB &= ~(1<<2); //set chip select pin low
  SPI_transfer_blocking(0x03); //send the command to read from the flash
  SPI_transfer_blocking((flash_address>>16)&0xff); //send to the flash the adress to be read from 
  SPI_transfer_blocking((flash_address>>8)&0xff);
  SPI_transfer_blocking((flash_address)&0xff);
  for(int iByte = 0; iByte<flash_buff_size; iByte++){
    flashbuff[iByte] = SPI_transfer_blocking(0x00); //read from the flash and save what's read to the buffer
  }
  PORTB |= (1<<2); //set chip select pin high
  flash_address+=flash_buff_size;  
  
  check_WIP; 
}

//------------------------------------------------------------------------------
void cmd_faw(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  // Serial.println("faw:");    
  if(arg_size[0] < 1 || arg_size[0] > 6 ){
    UART_print("improper size");
    return;  
  } 
  for(int iAddress_bit = 4; iAddress_bit<(4+arg_size[1]); iAddress_bit++){
    if(!isxdigit(args[0][iAddress_bit])){
      UART_print("improper address");
      flash_address = 0;
      return; 
    }
  }
  flash_address = (strtoul(((char*)args[0]),NULL,16)); 
  UART_print("address:");
  UART_print_hex(flash_address);
  UART_print("");
}

//------------------------------------------------------------------------------
void cmd_fsw(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  // Serial.println("fsw:");      
  PORTB &= ~(1<<2); //set chip select pin low
  SPI_transfer_blocking(0x06);
  PORTB |= (1<<2); //set chip select pin high
  for(int delayval = 0; delayval < 10000; delayval++);    
  PORTB &= ~(1<<2); //set chip select pin low
  SPI_transfer_blocking(0x01);
  if(arg_size[0] != 2){
    UART_print("improper size");
    return;  
  }  
  SPI_transfer_blocking(strtoul(((char*)args[0]),NULL,16));    
  //SPI.transfer(0x00);
  PORTB |= (1<<2); //set chip select pin high
  
  check_WIP;  
}

//------------------------------------------------------------------------------
void cmd_far(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //Serial.println("far:");   
  if(arg_size[0] != 0){
    UART_print("improper size");
    return;  
  }   
  UART_print_hex(flash_address); 
}

//------------------------------------------------------------------------------
void cmd_fsr(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //Serial.println("fsr:");     
  if(arg_size[0] != 0){
    UART_print("improper size");
    return;  
  }   
  PORTB &= ~(1<<2); //set chip select pin low
  SPI_transfer_blocking(0x05);
  uint8_t data1 = SPI_transfer_blocking(0x00);
  UART_print_hex(data1); //this used to print in decimal
  PORTB |= (1<<2); //set chip select pin high
  
  check_WIP;  
}
//------------------------------------------------------------------------------
uint8_t flash_to_fpga[flash_to_fpga_size];

void cmd_fpga(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS]){
  if(program_interrupt){
    UART_print("Not programming fpga");
    return;
  }
  //UART_print("fpga_conf: ");
  UART_print("Resetting fpgas");
  flash_address = 0; //set the flash address to be read from to 0
  //Serial.end(); //prevent serial interrupts from slowing the data transfer
  PORTD &= ~FPGA_PROGRAMMING_CLOCK_PIN; //set the clock for the fpgas low before programming
  PORTD &= ~FPGA_PROGRAMMING_NCONFIG_PIN; //set the fpga nCONFIG pin low to reset the fpga  
  while(PIND & FPGA_PROGRAMMING_NSTATUS_PIN); //wait until nSTATUS is dropped low
  // UART_print("nStatus is low");
  PORTD |= FPGA_PROGRAMMING_NCONFIG_PIN;  //set the fpga nCONFIG pin high to finish resetting the fpga
  while(!(PIND & FPGA_PROGRAMMING_NSTATUS_PIN)); //wait until nSTATUS is  high
  //UART_print("Finished resetting fpgas");

  //WHY ISN'T CONF DONE PULLED UP LATER?

  PORTB &= ~FLASH_CHIP_SELECT_PIN; // write FLASH_CHIP_SELECT_PIN (chip select for the flash) low so the flash can be read
  
  SPI_transfer_blocking(0x03); //Tell the flash which address will be read from initially
  SPI_transfer_blocking((flash_address>>16)&0xff); 
  SPI_transfer_blocking((flash_address>>8)&0xff);
  SPI_transfer_blocking((flash_address)&0xff); 
  
  PORTB &= FPGA_PROGRAMMING_DATA_PIN; //Bring the datapin high before programming begins
  //--INIT_DONE and CONF_DONE should be low by this point--
  // UART_print("begin sending data"); 

  SPI_transfer_non_blocking(0x00);
  while(!(SPSR & (1<<SPIF)));  // wait until transmission is complete
  flash_to_fpga[0]= SPDR; //read 1 byte from the flash


  while (!(PIND & FPGA_PROGRAMMING_CONF_DONE_PIN)) { //while CONF DONE is low 
    flash_address++; //increment the flash adress
    if(flash_address > 0x171240){ //if trying to program a place in memory 2x the size of the INTTDC file
      UART_print("tried to program for too long");
      return;
    }
    SPI_transfer_non_blocking(0x00);
    send_bytes(flash_to_fpga, 1); //send the data byte read from the flash to the fpga
    while(!(SPSR & (1<<SPIF)));  // wait until transmission is complete
    flash_to_fpga[0]= SPDR; //read 1 byte from the flash
  }

  //I think there is a problem with conf done, because programming would occur in seconds if fpga_conf_done rose 
   
   
  PORTB |= FLASH_CHIP_SELECT_PIN; // write FLASH_CHIP_SELECT_PIN (chip select for the flash) high after the flash has been read
  UART_print("data has been sent, configuration is done");
  PORTB &= ~FPGA_PROGRAMMING_DATA_PIN;//set FPGA_PROGRAMMING_DATA_PIN low 
  while(!(PIND & FPGA_PROGRAMMING_INIT_DONE_PIN)){ //while INIT_DONE is low
    //--This runs the clock 1 time, and sets the data low
    PORTD &= ~FPGA_PROGRAMMING_CLOCK_PIN;//digitalWrite(DCLK, 0);
    PORTD |= FPGA_PROGRAMMING_CLOCK_PIN;//digitalWrite(DCLK, 1);
  }  
  flash_address = 0;
  //Serial.begin(57600);
  return;   
  }

//------------------------------------------------------------------------------
void cmd_fpga_check(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  if((PIND & FPGA_PROGRAMMING_INIT_DONE_PIN)){
    UART_print("fpgas are configured");
  }else{
    UART_print("fpgas are NOT configured");
  }
}

//------------------------------------------------------------------------------
void cmd_temp(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  ADCSRA |= (1 << ADSC);  // Start A2D Conversions
  while(ADCSRA & (1<<ADSC));
  uint8_t low_voltage= ADCL;
  uint8_t high_voltage= ADCH;
  //UART_print_noline("0x");
  UART_print_hex(high_voltage);
  UART_print_hex(low_voltage);

  //(voltage/1024)*(3300/20) is the temperature in celsius 
}
