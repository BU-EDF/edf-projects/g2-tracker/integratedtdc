#ifndef __CMDS_H__
#define __CMDS_H__
#include <stdint.h>
#define MAX_ARGS 4
//#include "cmds.h"
void cmd_echo(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_fi(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_fe(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_fw(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_fc(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_fww(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_fwr(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_fr(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_faw(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_fsw(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_far(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_fsr(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_fpga(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_fpga_check(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_temp(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS]);

#endif
