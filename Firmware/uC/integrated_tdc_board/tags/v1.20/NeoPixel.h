/*--------------------------------------------------------------------
This code is a drastically scaled back version of the Adafruit NeoPixel library.
This is inteded only for the g-2 Tracker Integrated TDC board.
  --------------------------------------------------------------------*/

/*--------------------------------------------------------------------
  This file is part of the Adafruit NeoPixel library.

  NeoPixel is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of
  the License, or (at your option) any later version.

  NeoPixel is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with NeoPixel.  If not, see
  <http://www.gnu.org/licenses/>.
  --------------------------------------------------------------------*/

#ifndef NEOPIXEL_H
#define NEOPIXEL_H

#include <stdint.h>

#define PIXEL_COUNT 3
#define PIXEL_BYTE 3
#define PIXEL_PIN 0
#define PIXEL_PORT PORTC

void NeoPixel_show(void);
void NeoPixel_setPixelColor(uint16_t n, uint8_t r, uint8_t g, uint8_t b);
void NeoPixel_clear();
uint8_t *NeoPixel_getPixels(void);
inline int NeoPixel_canShow(void) { return 1; /*(micros() - endTime) >= 50L;*/ }



#endif // ADAFRUIT_NEOPIXEL_H
