#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <avr/pgmspace.h>
#include "UART_funcs.h"
#include "utils.h"
#include "cmds.h"
#include "SPI_cmds.h"
#include "NeoPixel.h"

#define COMMAND_SIZE 32

#define TOTAL_FLASH_SIZE 4194304 //size of the memory in bytes == 0x400000

#define PROMPT_CHARACTER '>'

//THE PINS MUST BE THE ONES ON THE MICROCONTROLLER 

#define CMD_COUNT 20

typedef void (*cmd_pointer)(char*[MAX_ARGS],uint8_t[MAX_ARGS]);

void cmd_show_ptrs(char*[MAX_ARGS],uint8_t[MAX_ARGS]);
void cmd_help(char*[MAX_ARGS],uint8_t[MAX_ARGS]);

const char CMD_echo_name[] PROGMEM = "echo";
const char CMD_fi_name[] PROGMEM = "fi";
const char CMD_fe_name[] PROGMEM = "fe";
const char CMD_fw_name[] PROGMEM = "fw";
const char CMD_fc_name[] PROGMEM = "fc";
const char CMD_fww_name[] PROGMEM = "fww";
const char CMD_fwr_name[] PROGMEM = "fwr";
const char CMD_show_ptrs_name[] PROGMEM = "ptrs";
const char CMD_fr_name[] PROGMEM = "fr";
const char CMD_faw_name[] PROGMEM = "faw";
const char CMD_fsw_name[] PROGMEM = "fsw";
const char CMD_far_name[] PROGMEM = "far";
const char CMD_fsr_name[] PROGMEM = "fsr";
const char CMD_conf_fpga_name[] PROGMEM = "conf_fpga";
const char CMD_help_name[] PROGMEM = "help";
const char CMD_fpga_check_name[] PROGMEM = "checkfpga";
const char CMD_temp_name[] PROGMEM = "temp";
const char CMD_led_name[] PROGMEM = "led";
const char CMD_fancy_on_name[] PROGMEM = "fancy_on";
const char CMD_version_name[] PROGMEM = "version";


const char * const cmd_names[CMD_COUNT] PROGMEM = {CMD_echo_name, CMD_fi_name, CMD_fe_name, CMD_fw_name, CMD_fc_name, CMD_fww_name, CMD_fwr_name, CMD_show_ptrs_name, CMD_fr_name, CMD_faw_name, CMD_fsw_name, CMD_far_name, CMD_fsr_name, CMD_conf_fpga_name, CMD_help_name, CMD_fpga_check_name, CMD_temp_name, CMD_led_name, CMD_fancy_on_name, CMD_version_name};
const cmd_pointer cmds[CMD_COUNT] PROGMEM = {&cmd_echo, &cmd_fi, &cmd_fe, &cmd_fw, &cmd_fc, &cmd_fww, &cmd_fwr, &cmd_show_ptrs, &cmd_fr, &cmd_faw, &cmd_fsw, &cmd_far, &cmd_fsr, &cmd_conf_fpga, &cmd_help, &cmd_fpga_check, &cmd_temp, &cmd_led, &cmd_fancy_on, &cmd_version};


static char command_line[COMMAND_SIZE+MAX_ARGS+1]; //add an additional byte for a null terminator
static uint8_t char_count = 0;
char empty_string[] = "";
char * empty_strings[4] = {empty_string,empty_string,empty_string,empty_string};
static char * positionbuff[MAX_ARGS];
static uint8_t sizebuff[MAX_ARGS];

uint8_t char_to_read = 0;

uint32_t micros_value = 0;

//------------------------------------------------------------------------------
void print_micros(void);
void initialize_fpga(void);
void buffer_overflow(void);
void process_char(void);
void command_run(void);
void tokenize(void);
void shift_command_line(int);
uint8_t cmd_cmp(void);

uint8_t pixel_number = 0;
uint8_t red_value_0 = 0;
uint8_t green_value_0 = 0;
uint8_t blue_value_0 = 0;

uint8_t led_switch = 1;
uint16_t seconds_elapsed = 0;

uint8_t fpga_status = 0; //initially the fpgas are unprogrammed

int main (void)
{
  UART_setup();
  SPI_setup();
  timer_setup();
    
  memset(command_line,0,sizeof(command_line));
  memcpy(positionbuff,empty_string,MAX_ARGS);
  memset(sizebuff,0,MAX_ARGS);
  
  //fill the local buffer that will be written to the flash to all F's
  cmd_fc(positionbuff,sizebuff);

  pin_setup();


  for(;pixel_number < 3;pixel_number++){
    NeoPixel_setPixelColor(pixel_number, red_value_0, green_value_0, blue_value_0); //set all three pixels to be off
  }
  NeoPixel_show();

  pixel_number = 0;

  initialize_fpga();

  while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR0 = PROMPT_CHARACTER; //send the prompt character

  for (;;) // Loop forever
    {
      if(UART_char_to_read_check()){ //check if a character is waiting to be processed (has been sent but not read)
	process_char();  
      }
      
      if((PIND & FPGA_PROGRAMMING_CONF_DONE_PIN)){
	fpga_status = 1;
      }else{
	fpga_status = 0;
      }

      if(fpga_status){ //set first led to blink green if fpgas were properly configured
	red_value_0 = 0;
	green_value_0 = 16;
	blue_value_0 = 0;
      }else{ //set first led to blink red if not
	red_value_0 = 16;
	green_value_0 = 0;
	blue_value_0 = 0;
      }
      cli();
      if(led_switch){ //flash led
	NeoPixel_setPixelColor(0, red_value_0, green_value_0, blue_value_0); //set first pixel
	NeoPixel_setPixelColor(1,0,0,0);
	NeoPixel_setPixelColor(2,0,0,0);
      }else{
	NeoPixel_clear();
      }
      NeoPixel_show();
      sei();
    }
  return 0; 
}

//------------------------------------------------------------------------------
ISR(TIMER0_OVF_vect)
{
  micros_value++;
}

//------------------------------------------------------------------------------
ISR(TIMER1_OVF_vect)
{
  if(led_switch){
    seconds_elapsed++;
  }
  led_switch ^= 1;
}

//------------------------------------------------------------------------------
void print_micros()
{
  UART_print_hex(micros_value);
}

//------------------------------------------------------------------------------
void initialize_fpga()
{
  for(int fpga_loop = 0;fpga_loop<2;fpga_loop++){ //try to program fpgas up to 2 times
    cmd_conf_fpga(positionbuff, sizebuff);             
    if((PIND & FPGA_PROGRAMMING_INIT_DONE_PIN)){  //if conf_done is high, programming is done
      break;
    }else{
      UART_print("fpgas were not programmed");
    }
  }
}
//------------------------------------------------------------------------------
void cmd_help(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //char** cmds_ptr = (char**)pgm_read_word(cmd_names);
  for(uint8_t command_number = 0;command_number < CMD_COUNT; command_number++){  
    char* string = (char*)pgm_read_word(cmd_names+command_number);
    char character = pgm_read_byte(string);
    while(character != '\0')
      {
	while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
	UDR0 = character;
	character = pgm_read_byte(++string);
       }	 
     UART_print("");
  }
  //print_UART(); //print microseconds elapsed
  //micro_print_hex(seconds_elapsed); //print seconds elapsed
}

//------------------------------------------------------------------------------
void cmd_show_ptrs(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  char* cmd_address = (char*)&(command_line[0]);
  UART_print_hex((uint16_t)cmd_address);
  UART_print_noline("   ");

  for(int iChar = 0;iChar<char_count;iChar++){
    while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
    if(command_line[iChar] != 0x00){
      UDR0 = command_line[iChar];
    }
    else{
      UDR0 = '~'; //print ~
    }
    //(I think this is redundant) while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  }

  UART_print("");

  for(uint8_t argnum = 0; argnum<MAX_ARGS; argnum++){
    if(arg_size[argnum] > 0){
      UART_print_hex(argnum);
      UART_print_noline("   ");
      UART_print_hex(sizebuff[argnum]);
      UART_print_noline("   ");
      char* pos_address = (char*)(positionbuff[argnum]);
      UART_print_hex((uint16_t)pos_address);
      UART_print_noline("   ");
      UART_print(positionbuff[argnum]);
    }
  }
}

//------------------------------------------------------------------------------
void buffer_overflow(void) 
{
  UART_print("    buffer overflow");  
  memset(command_line,0,sizeof(command_line));
  char_count = 0;
  UART_newline();
  while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR0 = PROMPT_CHARACTER; // send '>' character
  while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR0 = '\a';//send the bell character
}

//------------------------------------------------------------------------------
void command_run(void)
{ 
  tokenize();
  // command_line[char_count] = 0x0; 
  UART_newline();
  uint8_t cmd_num = 0;
  cmd_num = cmd_cmp();
  if((cmd_num < 0) || (cmd_num >= sizeof(cmds))){
    UART_print("Bad Command");
  }
  else {
    cmd_pointer entered_command = (cmd_pointer)pgm_read_word(cmds + cmd_num);
    entered_command(positionbuff,sizebuff);
  }
  UART_newline();
  char_count = 0;
  while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR0 = PROMPT_CHARACTER; // send '>' character
  memset(command_line,0,sizeof(command_line));
}

//------------------------------------------------------------------------------
inline void process_char(void)
{
  command_line[char_count] = UART_increment_interrupt_read();
  if(command_line[char_count] == 0x8){ //if backspace has been entered
    if(char_count > 0){
      command_line[char_count] = 0x0;
      backspace_func();
      char_count--;
    }
  }else if (command_line[char_count] == 0xA || command_line[char_count] == 0xD){ //if newline or carriage return is entered
    command_run();
  }else if(char_count == 32){ //if too long a command has been entered
    buffer_overflow();
  }else if (command_line[char_count] < 0x80 && command_line[char_count] >= 0x20){
    while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it 
    UDR0 = command_line[char_count]; // Echo back the received byte back to the computer
    char_count++;
  }else{ //if an invalid character is entered
    command_line[char_count] = 0x0; //change invalid character to a null character
    // UART_print("invalid character");
  }
}
//------------------------------------------------------------------------------
void tokenize(void)
{
  memcpy(positionbuff,empty_string,MAX_ARGS); //clear positionbuff
  memset(sizebuff,0,MAX_ARGS); //clear sizebuff
  uint8_t temp_sizebuff[MAX_ARGS+2]; //5 large to also store the command
  memset(temp_sizebuff,0,MAX_ARGS+2); //clear temp_sizebuff
  char * temp_positionbuff[MAX_ARGS+1];
  memcpy(temp_positionbuff,empty_string,MAX_ARGS+1); //clear positionbuff
  int argument_value = 0;
  
  uint8_t command_line_position = 0;
  char current_char;
  
  if (command_line[char_count-1] != ' '){  //if a space  wasn't entered last
    command_line[char_count] = ' '; // add a space  to the end
    char_count++;
  } 

  //command loop
  for (; command_line_position<char_count+1; command_line_position++){
    current_char = command_line[command_line_position] = to_lower(command_line[command_line_position]); //change all uppercase characters to lowercase
    if (current_char  == 0x00){ //if a null terminator is entered before any space
      return;
    }else if (current_char  == ' '){ //tokenizing will begin if a space is entered
      command_line[command_line_position]= 0x00; //change the space to a null terminator
      command_line_position++; //move on to the next character before breaking the loop
      break;
    }
  }
  //argument loop
  uint8_t parsing_arg = 0; //0 if waiting for an argument, 1 if currently parsing argument
  for (; command_line_position<char_count+1; command_line_position++){
    current_char = command_line[command_line_position] = to_lower(command_line[command_line_position]); //change all uppercase characters to lowercase
    if (parsing_arg == 0){ //if waiting to find the first char of an argument
      if (current_char == 0x00){ //if a null terminator is entered before any space
	return;
      }else if (current_char == ' '){ //tokenizing will begin if a space is entered
        command_line[command_line_position] = 0x00; //change the space to a null terminator
      }else {//if a valid character is entered
	positionbuff[argument_value] = command_line+command_line_position; //set positionbuff
	parsing_arg = 1;
	sizebuff[argument_value]++;
      }
    }else { //if currently parsing an argument
      if (current_char == 0x00){ //if a null terminator is entered before any space
	return;
      }else if (current_char == ' '){ //tokenizing will begin if a space is entered
        command_line[command_line_position] = 0x00; //change the space to a null terminator
       	argument_value++;
	parsing_arg = 0;
	if (argument_value > MAX_ARGS){ //there cannot be more than MAX_ARGS arguments
	  UART_print ("too many arguments");
	  return;
	}
      }else if (sizebuff[argument_value] == 8){ //if the current arg is too long, split it up
	shift_command_line(command_line_position);
	command_line_position++;
	argument_value++; //start a new argument
	sizebuff[argument_value]++; //set sizebuff
	positionbuff[argument_value] = command_line+command_line_position; //set positionbuff
      }else { //if the character is valid
	sizebuff[argument_value]++; //set sizebuff
      }
    } 
  }
}
//------------------------------------------------------------------------------
void shift_command_line(int command_line_position){
  //UART_print(" 8 long");
  for(int char_shift = char_count+1; char_shift>command_line_position; char_shift--){ 
    command_line[char_shift] = command_line[char_shift-1];
  }
  command_line[command_line_position] = 0x0; // null terminate the string
  char_count++; //increase the length of the string entered to command line by 1
}

//------------------------------------------------------------------------------
uint8_t cmd_cmp(void)
{
  for(uint8_t command_number = 0;command_number < CMD_COUNT; command_number++){  
    if(strcmp_P(command_line,(char*)pgm_read_word(cmd_names+command_number)) == 0){ //if the strings are the same
      return command_number; //return which command was matched
    }
  }
  return -1;
}
