// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            A0

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      3

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int delayval = 500; // delay for half a second

#define buffsize  128 //number of bytes to be writen to and read from the flash per read/write
#define datasize  100
#define flash_to_fpga_size 1
#define commandsize 32
#define positionbuffsize 32
#define sizebuffsize 32


//THE PINS MUST BE THE ONES ON THE MICROCONTROLLER 
#define fpga_data_pin B10000000 //DATA is out on pin B7 (--)
#define fpga_clock_pin B00010000 //DCLK is out on pin D4 (4)
#define flash_chip_select_pin B00000100 //slave select is pin B2 (10)
#define fpga_nConfig_pin B00100000 //nConfig is on pin D5 (5)
#define fpga_init_done_pin B00010000 //init_done is pin C4 (A4)
#define fpga_conf_done_pin B10000000 //CONF_DONE is on pin D7 (7)
#define fpga_nStatus_pin B01000000 //nStatus is on pin D6 (6)
#define fpga_test_pin_1 B00000010 //test is pin C1 (A1)

#include <SPI.h>
#include <ctype.h>

int key = 0;
int DATA = A1;
int DCLK = 4;
int nCONFIG = 5;
int INIT_DONE = A4;
int CONF_DONE = 7;
int nSTATUS = 6;
int flashchipselectpin = 10;

uint32_t bytes_read = 0; 
uint8_t data[datasize];
uint8_t flash_to_fpga[flash_to_fpga_size];
uint8_t command[commandsize]; 
uint8_t positionbuff[positionbuffsize];
uint8_t sizebuff[sizebuffsize];
uint8_t flashbuff[buffsize];
uint32_t flash_address = 0;

//---------------------------------------------------------------------------------------------------------\\

void setup(){
   // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
  #if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  #endif
  // End of trinket special code

  pixels.begin(); // This initializes the NeoPixel library.
 
  memset(data,0,datasize); 
  memset(flash_to_fpga,0,flash_to_fpga_size);
  memset(command,0,commandsize);
  memset(positionbuff,0,positionbuffsize);
  memset(sizebuff,0,sizebuffsize);
  memset(flashbuff,0,buffsize);
  
  PORTD |= fpga_nConfig_pin;
  //PORTD |= fpga_conf_done_pin;
  PORTD &= ~fpga_clock_pin;
  DDRD = (fpga_clock_pin | flash_chip_select_pin | fpga_nConfig_pin);
  //pinMode (flashchipselectpin, OUTPUT); // set the flashchipselectpin as an output:
  PORTB |= fpga_data_pin;
  DDRB |= fpga_data_pin; //pinMode(DATA, OUTPUT);
  DDRC |= fpga_test_pin_1;
  ASSR &= ~B00100000;


  for(int iBit = 0; iBit<buffsize; iBit++){
    flashbuff[iBit] = 0xff; //set every bit in the flashbuff to 1, because the flash is initialized to 1's
  }  

  SPI.begin(); // initialize SPI
  Serial.begin(250000); //initialize Serial
  Serial.println();
  Serial.println("Integrated TDC board v 0.1");
  check_WIP();
}

//---------------------------------------------------------------------------------------------------------\\

void loop(){
  //create a buffer  
  /*if(!(PORTD &= CONF_DONE)){ //if CONF_DONE is not high, configure the fpga from the flash (I am not confident this syntax is correct, so for now the if statement is commented out)
    run_fpga_conf();
  }else*/ if (Serial.available() > 0){  //otherwise intepret commands
    command[key] = Serial.read();    
    if((command[key] == 0xA) || // Line feed
       (command[key] == 0xD)){  // Carriage Return
      //If there's a newline, analyze and run the command that was typed
      Serial.println();
      initializefct();
      tokenize();    
      run_command();
      Serial.print(">"); 
      uint8_t command[] = {0}; 
      key = 0;     
    }else if(command[key] == 0x8){ //if backspace key is pressed
      backspacefct();
    }else if ((command[key] >= 32) || // space
             (command[key] <0X7F)){  // delete
      Serial.write(command[key]);
      key++;
      if(key == 32){ //if more than 32 characters are entered, clear the line
        Serial.println("  buffer overflow");
        Serial.print(">");
        uint8_t command = {0}; 
        key = 0;
      }
    }
  }
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited  
//reset position and size buffers
void initializefct(){
  for(int z = 0; z<32;z++){
    positionbuff[z] = 0;
    sizebuff[z] = 0;
  }
  return; 
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited  
//function for backspace
void backspacefct(){
  if (key>0){
    Serial.write(0x8); //write backspace
    Serial.write(' ');
    Serial.write(0x8);
    key--;
  }
  return;
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited  
//function to split written commands into distinct strings
void tokenize(){
  int location = 0;
  positionbuff[location] = 0;
  if (command[key-1] != 0x20){  //if a space wasn't entered last
    command[key] = 0x20; // add a space to the end
  }  
  for(int z = 0; z<key+1; z++){ 
    if (command[z] == 0x20){ //if at a space
      command[z] = 0x0; // null terminate the string
      location++;
      positionbuff[location] = z+1; //set the location of the first character in the string
      sizebuff[location-1] = positionbuff[location]-positionbuff[location-1]-1; //set the size of the string
    }    
    else if(z==(positionbuff[location]+8)){ //if more than 8 characters into a string
      for(int x = key+1; x>z; x--){ 
        command[x] = command[x-1];
      }
      key+=1;
      command[z] = 0x0; // null terminate the string
      location++;
      positionbuff[location] = z+1; //set the location of the first character in the string
      sizebuff[location-1] = positionbuff[location]-positionbuff[location-1]-1; //set the size of the string
      //positionbuff[location]-=1;    
    } 
  }
  return;  
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited  
//function to change upercase letters to lowercase, for comparing strings.
uint8_t to_lower(uint8_t letter){
  if(letter<91 && letter>64){ 
    return letter+0x20;
  }
  else{
    return letter;
  }
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited  
//function to find the length of a string
int string_size(const char* cmd){
  int strlength = 0; 
  for(int n=0; n<100; n++){
    if (cmd[n] !=0){
      strlength++;
    }
    else{
      return strlength; 
    }
  } 
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited  
//function to compare an entered command to cmd, and return true if they match
int command_cmp(const char* cmd){
  uint8_t max_size = sizebuff[0];
  //Serial.print(string_size(cmd));
  //Serial.print(" ");
  //Serial.println(max_size);  
  if (max_size != string_size(cmd)){
    return 0;
  }
  if (max_size ==0){
    return 0;
  }
  for(int i = 0; i<max_size; i++){
    if (to_lower(command[i]) != cmd[i]){
      return 0;      
    }
  }  
  return 1;
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited  
//Read the status register and return its current value
uint8_t statusreg(){
  digitalWrite(flashchipselectpin,LOW);
  SPI.transfer(0x05);
  uint8_t data1 = SPI.transfer(0x00);
  digitalWrite(flashchipselectpin,HIGH);
  
  return data1;
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited  
//print "bad command" to the screen.
void bad_command(){
  Serial.println("Bad Command");
  return;
} 

//---------------------------------------------------------------------------------------------------------\\
//delay until bit 0 of the status register is 0 
void check_WIP(){
  while (statusreg()&0x01);
}
//---------------------------------------------------------------------------------------------------------\\
//This is where all commands can added or removed 
void run_command(){
  if (command_cmp("echo")){ //Echo
    run_echo();
  }else if(command_cmp("fi")){ //Flash info
    run_fi();
  }else if(command_cmp("fe")){ //Flash erase
    run_fe();
  }else if(command_cmp("fw")){ //Flash write
    run_fw();
  }else if(command_cmp("fc")){ //Flash clear
    run_fc();
  }else if(command_cmp("fww")){ //Flash write word
    run_fww();
  }else if(command_cmp("fwr")){ //Flash word read
    run_fwr();
  }else if(command_cmp("fr")){ //Flash read
    run_fr();
  }else if(command_cmp("faw")){ //Flash address write
    run_faw();
  }else if(command_cmp("fsw")){ //Flash status write
    run_fsw();
  }else if(command_cmp("far")){ //Flash address read
    run_far();  
  }else if(command_cmp("fsr")){ //Flash status read
    run_fsr();
  }else if(command_cmp("fpga")){ //reconfigure fpga
    run_fpga_conf();  
  }else if(command_cmp("led")){ //set leds
    run_set_leds();
  }else{ 
    bad_command();
  }
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited  
//echo to screen. Almost only for debugging
void run_echo(){
  Serial.println("ECHO:");  
  for (int z = 5; z<key; z++){
    if(command[z] == 0x0){
      Serial.write(0x20);  
    }
    else{
      Serial.write(command[z]);
    }
  }
  Serial.println();        
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited  
//gets and checks info from the flash
void run_fi(){
  //Serial.println("fi: ");
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  Serial.println("Buffer size: 128 bytes");   
  digitalWrite(flashchipselectpin,LOW);
  SPI.transfer(0x9F);
  uint8_t data1 = SPI.transfer(0x00);
  uint8_t data2 = SPI.transfer(0x00);
  uint8_t data3 = SPI.transfer(0x00);
  digitalWrite(flashchipselectpin,HIGH);
  if((data1 == 0x20) && (data2 == 0x20) && (data3 == 0x16)){
    Serial.println("info is correct");
  }
  else{
    Serial.println("info is incorrect");
  }      
}

//---------------------------------------------------------------------------------------------------------\\
//erases the flash for writing  
void run_fe(){
  //Serial.println("fe:");   
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  digitalWrite(flashchipselectpin,LOW);
  SPI.transfer(0x06);    
  digitalWrite(flashchipselectpin,HIGH); 
  /*while (statusreg() != 2){
  //Serial.println(statusreg());
    delay(500);  
    digitalWrite(flashchipselectpin,LOW);
    SPI.transfer(0x06);    
    digitalWrite(flashchipselectpin,HIGH); 
    delay(500);      
  }*/
  digitalWrite(flashchipselectpin,LOW);
  SPI.transfer(0xC7);
  digitalWrite(flashchipselectpin,HIGH);  
  
  check_WIP();
}

//---------------------------------------------------------------------------------------------------------\\
//write big buffer of words into flash memory 
void run_fw(){
//  Serial.println("fw:");
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  digitalWrite(flashchipselectpin,LOW);
  SPI.transfer(0x06);    
  digitalWrite(flashchipselectpin,HIGH); 
  /*while (statusreg() != 2){
    //Serial.println(statusreg());
    delay(500);  
    digitalWrite(flashchipselectpin,LOW);
    SPI.transfer(0x06);    
    digitalWrite(flashchipselectpin,HIGH); 
    delay(500);      
  }*/
  digitalWrite(flashchipselectpin,LOW);
  SPI.transfer(0x02); //send the command to program the flash
  SPI.transfer((flash_address>>16)&0xff); //send to the flash the address to be read from
  SPI.transfer((flash_address>>8)&0xff);
  SPI.transfer((flash_address)&0xff);
  for(int z = 0; z<buffsize; z++){
    SPI.transfer(flashbuff[z]);
  }
  digitalWrite(flashchipselectpin,HIGH);
  flash_address+=buffsize;  
  
  check_WIP; 
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited  
//clear large flash buffer
void run_fc(){
  //Serial.println("flash buffer cleared");     
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  for(int z = 0; z<buffsize; z++){
    flashbuff[z] = 0xff;
  }  
}

//---------------------------------------------------------------------------------------------------------\\
//write the given word to a big buffer to be sent to the flash
void run_fww(){
//  Serial.println("fww:");
//  strtol("0x7", NULL, 0);
  if( (sizebuff[1] == 1)   && // argument 1 is one character long
      isxdigit(command[4]) && // arguments 1 is a hex digit
      (sizebuff[2] == 8)   && // argument 2 is 8 chars long (1 32bit word)
      (sizebuff[3] == 8) ){   // argument 3 is 8 chars long
    int offset = strtoul((char*)&(command[4]),NULL,16)*8;  //offset in memory for this 64bit word
    unsigned long low_word = strtoul((char*)&(command[positionbuff[3]]),NULL,16);
    unsigned long high_word = strtoul((char*)&(command[positionbuff[2]]),NULL,16);
    
    Serial.println(low_word, HEX);
    Serial.println(high_word, HEX);
    for(int iByte=0; iByte<8; iByte++){      
      if (iByte < 4){
        flashbuff[offset + iByte] = (low_word >> iByte*8)&0xFF;
      }else{
        flashbuff[offset + iByte] = (high_word >> (iByte-4)*8)&0xFF;
      Serial.println(flashbuff[offset+iByte], HEX); 
    }
    }
  }
  else{
    Serial.println("improper size");
    return;  
  }   
}

//---------------------------------------------------------------------------------------------------------\\
//print the big buffer of words ready to be written to the flash
void run_fwr(){
  Serial.println("fwr:");
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }      
  for(int y = 0; y<(buffsize/8);y++){
    for(int iByte = 7; iByte>=0; iByte--){
      Serial.print(flashbuff[(y*8)+iByte], HEX);
      //Serial.print(' '); //space between HEX words. optional
    }
    Serial.println(); 
  }  
}

//---------------------------------------------------------------------------------------------------------\\
//read the flash at the address into the 128 byte buffer on the uC (address is auto incremented) 
void run_fr(){
  Serial.println("fr:");   
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }     
  digitalWrite(flashchipselectpin,LOW);
  SPI.transfer(0x03); //send the command to read from the flash
  SPI.transfer((flash_address>>16)&0xff); //send to the flash the adress to be read from 
  SPI.transfer((flash_address>>8)&0xff);
  SPI.transfer((flash_address)&0xff);
  for(int iByte = 0; iByte<buffsize; iByte++){
    flashbuff[iByte] = SPI.transfer(0x00); //read from the flash and save what's read to the buffer
  }
  digitalWrite(flashchipselectpin,HIGH);
  flash_address+=buffsize;  
  
  check_WIP;   
}

//---------------------------------------------------------------------------------------------------------\\
//Set the flash address to write to or read from
void run_faw(){
  Serial.println("faw:");    
  for(int iAddress_bit = 4; iAddress_bit<(4+sizebuff[2]); iAddress_bit++){
    if(!isxdigit(command[iAddress_bit])){
      Serial.println("improper address");
      flash_address = 0;
      return; 
    }
  }
    flash_address = (strtoul(((char*)command+4),NULL,16)); 
  Serial.println("address:");
  Serial.println(flash_address, HEX);   
}

//---------------------------------------------------------------------------------------------------------\\
//write to the status register of the flash  
void run_fsw(){
  Serial.println("fsw:");      
  //while (statusreg() != 2){
  //Serial.println(statusreg()); 
  digitalWrite(flashchipselectpin,LOW);
  SPI.transfer(0x06);
  digitalWrite(flashchipselectpin,HIGH);
  delay(1000);    
  //}
  digitalWrite(flashchipselectpin,LOW);
  SPI.transfer(0x01);
  if(sizebuff[1] != 2){
    Serial.println("improper size");
    return;  
  }  
  SPI.transfer(strtoul(((char*)command+4),NULL,16));    
  //SPI.transfer(0x00);
  digitalWrite(flashchipselectpin,HIGH);   
  
  check_WIP;   
}

//---------------------------------------------------------------------------------------------------------\\
//print back the current address to be written to or read from 
void run_far(){
  Serial.println("far:");   
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  Serial.println(flash_address, HEX);  
}

//---------------------------------------------------------------------------------------------------------\\
//read the status register of the flash  
void run_fsr(){
  Serial.println("fsr:");     
  if(sizebuff[1] != 0){
    Serial.println("improper size");
    return;  
  }   
  digitalWrite(flashchipselectpin,LOW);
  SPI.transfer(0x05);
  uint8_t data1 = SPI.transfer(0x00);
  Serial.println(data1, DEC);
  digitalWrite(flashchipselectpin,HIGH);
  
  check_WIP;  
}

//---------------------------------------------------------------------------------------------------------\\
//configure the fpga
//I'm not exactly sure why the chech_WIP commands are neccessary, but they fixed a problem I had.
void run_fpga_conf(){
  Serial.println("fpga_conf: ");
  Serial.println("Resetting fpgas");
  flash_address = 0; //set the flash address to be read from to 0
  //Serial.end(); //prevent serial interrupts from slowing the data transfer
  PORTD &= ~fpga_clock_pin; //set the clock for the fpgas low before programming
  PORTD &= ~fpga_nConfig_pin; //set the fpga nCONFIG pin low to reset the fpga  
  while(PIND & fpga_nStatus_pin); //wait until nSTATUS is dropped low
  Serial.println("nStatus is low");
  PORTD |= fpga_nConfig_pin;  //set the fpga nCONFIG pin high to finish resetting the fpga
  while(!(PIND & fpga_nStatus_pin)); //wait until nSTATUS is  high
  Serial.println("Finished resetting fpgas");

  //WHY ISN'T CONF DONE PULLED UP LATER?

  PORTB &= ~flash_chip_select_pin; // write flash_chip_select_pin (chip select for the flash) low so the flash can be read
  bytes_read = 0; //reset the number of bytes read from the flash to 0
 
  SPI.transfer(0x03); //Tell the flash which address will be read from initially
  SPI.transfer((flash_address>>16)&0xff); 
  SPI.transfer((flash_address>>8)&0xff);
  SPI.transfer((flash_address)&0xff); 
  
  PORTB &= fpga_data_pin; //Bring the datapin high before programming begins
  //--INIT_DONE and CONF_DONE should be low by this point--
  Serial.println("begin sending data"); 
  while (!(PIND & fpga_conf_done_pin)) { //while CONF DONE is low 
    flash_to_fpga[0]= SPI.transfer(0x00); //read 1 byte from the flash
    flash_address++; //increment the flash adress
    send_bytes(flash_to_fpga, 1); //send the data byte read from the flash to the fpga
    bytes_read++; //increment the number of bytes read
  }

  //I think there is a problem with conf done, because programming would occur in seconds if fpga_conf_done rose 
   
   
  PORTB |= flash_chip_select_pin; // write flash_chip_select_pin (chip select for the flash) high after the flash has been read
  Serial.println("data has been sent, configuration is 'done'");
  PORTB &= ~fpga_data_pin;//set fpga_data_pin low 
  while(!(PIND & fpga_init_done_pin)){ //while INIT_DONE is low
    //--This runs the clock 1 time, and sets the data low
    PORTD &= ~fpga_clock_pin;//digitalWrite(DCLK, 0);
    PORTD |= fpga_clock_pin;//digitalWrite(DCLK, 1);
  }  
  flash_address = 0;
  //Serial.begin(57600);
  return;   
}

//---------------------------------------------------------------------------------------------------------\\
//This shouldn't need to be edited
//function to send bytes to the fpga, with a clock
int send_bytes(uint8_t *data, int length){
  uint8_t *data_byte = data;
  uint8_t *data_end = data+length;
  while(data_byte < data_end){
    uint8_t local_data = *data_byte;
    for(int bits = 0; bits< 8; bits++){
        PORTD &= ~fpga_clock_pin; // change the clock
      //--if the data changed, set portD to the new bit, otherwise keep it the same
      if(local_data & 1){
        PORTB |= fpga_data_pin; 
      }
      else{
        PORTB &= ~fpga_data_pin; 
      }
        PORTD |= fpga_clock_pin; //change the clock again 
      local_data >>= 1; //shift to the next bit of data
    }
    data_byte++;
  }  
}

//---------------------------------------------------------------------------------------------------------\\
//set the leds to 3 different colors specified numerically 
int run_set_leds(){
  
  int led_num = command[positionbuff[2]];
  int Rvalue = command[positionbuff[3]];
  int Gvalue = command[positionbuff[4]];
  int Bvalue = command[positionbuff[5]];
    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    pixels.setPixelColor(led_num, pixels.Color(Rvalue,Gvalue,Bvalue)); // Moderately bright green color.
    pixels.show(); // This sends the updated pixel color to the hardware.

//    digitalWrite(A0,0);
    delay(delayval); // Delay for a period of time (in milliseconds).
}    
//---------------------------------------------------------------------------------------------------------\\
