#!/usr/bin/env python

import serial
import argparse
import difflib
import sys
import re

def WriteCommand(ser,line):
   if line.find("\n") != -1:
       raise cmd("new line in command")
   # write command and make sure the reponse makes sense                                                                                                                                                                                                                          
   for char in line:
           ser.write(char)
           rd = ser.read()
           if rd != char:
                   print "bad char! " + str(rd) + " is not expected " + str(char)
                   raise cmd("bad char")
#   ser.write(line)                                                                                                                                                                                                                                                               
#execute the command and read the output                                                                                                                                                                                                                                          
   ser.write("\n")
   rd = ser.read()
   #print "\"", rd, "\" :", hex(ord(rd))
   while rd != "\n":
       rd = ser.read()
       #print "\"", rd, "\" :", hex(ord(rd))
   rd = ser.read()
   #print "\"", rd, "\" :", hex(ord(rd))
   line = ""
   while rd != ">":
       line = line + rd
       rd = ser.read()
#    line = line.split()                                                                                                                                                                                                                                                          
   return line

# Inputs ******************************************************************************************************

parser = argparse.ArgumentParser(description='Program flash from intel hex file via serial. Default to 250000 baud rate on ttyUSB0.')

parser.add_argument('-b','--baud', help='Set baudrate. Default is 250000.', type=int, default=250000)
parser.add_argument('-D','--Device', help='Change USB port. Default is /dev/ttyUSB0', default="/dev/ttyUSB0")
args = vars(parser.parse_args())

baud_rate = args["baud"]
device = args["Device"]
# Main Function **********************************************************************************************

#open the serial port
# ser = serial.Serial("/dev/ttyUSB0",250000);
ser = serial.Serial(device, baud_rate);

WriteCommand(ser, "")

print WriteCommand(ser, "fpga")

