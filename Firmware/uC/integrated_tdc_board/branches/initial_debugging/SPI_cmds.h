#ifndef __SPI_CMDS_H__
#define __SPI_CMDS_H__
#include <avr/io.h>
#include <avr/interrupt.h>

void SPI_setup(void);

uint8_t SPI_transfer_blocking(uint8_t);

void SPI_transfer_non_blocking(uint8_t);

uint8_t statusreg(void);

void check_WIP(void);
#endif
