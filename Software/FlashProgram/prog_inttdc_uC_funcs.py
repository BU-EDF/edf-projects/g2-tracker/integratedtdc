#!/usr/bin/python

#import libraries
import time
import serial
import argparse


#should be connected to the Logicboard right now

def connect_to_inttdc(inttdc_board, ser):
   ser.write("tdc_prog")
   time.sleep(0.01)
   ser.write(inttdc_board)
   ser.write("\n")
   rd = ser.read()
   while rd != '\x0D': #wait until the carriage return
      rd = ser.read()

   time.sleep(0.01)

   ser.flushInput()
   # get in sync with the AVR
   for i in range(5):
      print "syncing"
      ser.write('\x30') # STK_GET_SYNC
      ser.write('\x20') # STK_CRC_EOP
      time.sleep(0.05)

   # receive sync ack
   print "receiving sync ack"
   insync = ser.read(1) # STK_INSYNC
   ok = ser.read(1) # STK_OK
   
   ### added
   print "insync = " + hex(ord(insync))
   print "ok = " + hex(ord(ok))
   
   ser.flushInput()
   ###
   
   # check received ack
   if insync == '\x14' and ok == '\x10':
      print "insync"
   else: 
      print "ERROR: not insync"
      exit()

   return

def get_inttdc_info(ser):
   # get the MAJOR version of the bootloader
   print "getting the MAJOR version of the bootloader"
   ser.write('\x41') # STK_GET_PARAMETER
   ser.write('\x81') # STK_SW_MAJOR
   ser.write('\x20') # SYNC_CRC_EOP
   time.sleep(0.05)
   
   # receive bootlader MAJOR version
   print "receiving bootloader MAJOR version"
   insync = ser.read(1) # STK_INSYNC
   major = ser.read(1) # STK_SW_MJAOR
   ok = ser.read(1) # STK_OK
   
   ser.flushInput()
   
   # check received sync ack
   if insync == '\x14' and ok == '\x10':
      print "insync"
   else: 
      print "ERROR: not insync"
      exit()

   # get the MINOR version of the bootloader
   print "getting the MINOR version of the bootloader"
   ser.write('\x41') # STK_GET_PARAMETER
   ser.write('\x82') # STK_SW_MINOR
   ser.write('\x20') # SYNC_CRC_EOP
   time.sleep(0.05)
   
   # receive bootlader MINOR version
   print "receiving bootloader MINOR version"
   insync = ser.read(1) # STK_INSYNC
   minor = ser.read(1) # STK_SW_MINOR
   ok = ser.read(1) # STK_OK
   
   # check received sync ack
   if insync == '\x14' and ok == '\x10':
      print "insync"
   else: 
      print "ERROR: not insync"
      exit()
      
   print "bootloader version %s.%s" % (ord(major), ord(minor))

   # enter programming mode
   print "entering programming mode"
   ser.write('\x50') # STK_ENTER_PROGMODE
   ser.write('\x20') # SYNC_CRC_EOP
   time.sleep(0.05)

   # receive sync ack
   print "receiving sync ack"
   insync = ser.read(1) # STK_INSYNC
   ok = ser.read(1) # STK_OK
   
   # check received sync ack
   if insync == '\x14' and ok == '\x10':
      print "insync"
   else: 
      print "ERROR: not insync"
      exit()

   # get device signature
   print "getting device signature"
   ser.write('\x75') # STK_READ_SIGN
   ser.write('\x20') # SYNC_CRC_EOP

   # receive device signature
   print "receiving device signature"
   insync = ser.read(1) # STK_INSYNC
   signature = ser.read(3) # device
   ok = ser.read(1) # STK_OK
   
   # check received sync ack
   if insync == '\x14' and ok == '\x10':
      print "insync"
   else: 
      print "ERROR: not insync"
      exit()

   print "device signature %s-%s-%s" % (ord(signature[0]), ord(signature[1]), ord(signature[2]))

   return

def check_hex_file(hex_file, ser):
   # start with page address 0
   line_address = 0
   line_size = 0
   record_type = 0
   
   # open the hex file
   program = open(hex_file, "rb")
   
   data = ""
   # the hex in the file is represented in char
   # so we have to merge 2 chars into one byte
   
   #grab every line at once (all data) and check file for any corruption
   while True:
      sum_check = 0
      # just take the program data
      line = program.readline()
      if line[0] !=  ':':
         print "ERROR: incorrect file, no leading ':' "
         print line
         exit()
      original_line = line
      line_address = (int(line[3:7], 16))+16
      line_size = int(line[1:3], 16)
      if line_size  > 16:
         print "ERROR: incorrect file, line_size error"
         print original_line
         exit()
      if line_size == 0:
         break
      record_type = int(line[7:9], 16)
      if record_type != 0:
         print "ERROR: incorrect file, record_type error"
         print original_line
         exit()
      data += line[9:-4]
      if (len(line[9:-4])/2) != line_size:
         print "ERROR: incorrect file, not enough data on the line:"
         print original_line
         exit()
      line = line[1:]
   
      #sum the entire line
      while line != line[-2:]:
         # assemble a byte and add it 
         sum_check += (int(line[:2], 16))
         # chop of sent data
         line = line[2:]
      if (sum_check & 0xFF) != 0: 
         print "ERROR: incorrect file, check_sum error"
         print original_line
         exit()
         
      # close the hex file
   program.close()
   return data
   
def program_inttdc_uC_flash(data, ser):
   #program the flash with the data
   address = 0
   while True:
      # calculate page address
      laddress = chr((address) % 256)
      haddress = chr((address) / 256)  
      address += 64     
      
      # load page address
      print "loading page address"
      ser.write('\x55') # STK_LOAD_ADDRESS
      ser.write(laddress)
      ser.write(haddress)
      ser.write('\x20') # SYNC_CRC_EOP
      #time.sleep(0.01)
      
      # receive sync ack
      print "receiving sync ack"
      insync = ser.read(1) # STK_INSYNC
      ok = ser.read(1) # STK_OK
      
      # check received sync ack
      if insync == '\x14' and ok == '\x10':
	 print "insync"
      else: 
         print "ERROR: not insync"
         exit()
                
      if (len(data)/2) > 128:
         size = '\x80'
      else:
         size = chr(len(data)/2)
      print "writing program page to write", ord(size), "laddress", ord(laddress), "haddress", ord(haddress)
      ser.write('\x64') # STK_PROGRAM_PAGE
      ser.write('\x00') # page size
      ser.write(size) # page size 
      ser.write('\x46') # flash memory, 'F'
      # while data left
      byte_count = 0
      while ((byte_count < 128) and data):
	 # assemble a byte and write it
	 ser.write(chr(int(data[:2], 16)))
	 # chop of sent data
	 data = data[2:]
         byte_count+=1
      ser.write('\x20') # SYNC_CRC_EOP
      time.sleep(0.01)
      
      ser.flushInput()
      
      # receive sync ack
      print "receiving sync ack"
      insync = ser.read(1) # STK_INSYNC
      ok = ser.read(1) # STK_OK
      
      
      # check received sync ack
      if insync == '\x14' and ok == '\x10':
	 print "insync"
      else: 
         print "ERROR: not insync"
         exit()
         
      if size != '\x80':
         break
      
   return

def leave_bootloader(ser):
  
   # leave programming mode
   print "leaving programming mode"
   ser.write('\x51') # STK_LEAVE_PROGMODE
   ser.write('\x20') # SYNC_CRC_EOP
   time.sleep(0.05)
   
   # receive sync ack
   print "receiving sync ack"
   insync = ser.read(1) # STK_INSYNC
   ok = ser.read(1) # STK_OK
   
   # check received sync ack
   if insync == '\x14' and ok == '\x10':
      print "insync"
   else: 
      print "Finished programming"
      return
   # close the serial connection
   ser.close()
   print "ERROR: Integrated TDC uC not programmed"
   exit()
