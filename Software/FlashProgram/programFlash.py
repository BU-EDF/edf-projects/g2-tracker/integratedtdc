#!/usr/bin/env python

import serial
from array import *
import sys
import argparse

from prog_inttdc_uC_funcs import *

def find_address_bounds(infile):
	upper_addr = 0
	highest_address = -1
	lowest_address = -1
	for line in infile:

		line_string = line.strip().strip(":")

		line_check, byte_count, incremental_addr, record_type, data  = line_analysis(line)
		# Uses line analysis to find information and to ensure lines are not corrupt

		# # Line information *******
		# byte_count = int(line_string[0:2], 16)
		# incremental_addr = int(line_string[2:6], 16)
		# record_type = int(line_string[6:8], 16)
		# data = line_string[8:len(line_string)-2]
		# # ************************

		if line_check:
			#Record Types: 00 = data
			#			   01 = end of file
			#			   04 Extended Linear Address
			if record_type == 00:
				start_address = upper_addr + incremental_addr
				if highest_address == -1:
					highest_address = start_address
					lowest_address = start_address
				elif start_address > highest_address:
					highest_address = (start_address+(byte_count-1))
				elif start_address < lowest_address:
					lowest_address = start_address
			elif record_type == 04:
		 		upper_addr = (int(data,16)<<16)

		else:
		 	return -1, -1

	return highest_address,lowest_address



def line_analysis(raw_line):
	#print raw_line
	if not raw_line.strip(): # if blank line in file, skip over it
		return False, 0, 0, 0, 0

	if raw_line[0] != ':': # check for malformed data missing the leading marker
		print "LoadFLASH: BAD Line in file: ", raw_line, "(line does not begin with a ':' )"
		return False, 0, 0, 0, 0

	raw_line=raw_line.strip() # remove \n character from the end of the string
	line_string=raw_line.strip(":") # remove the ":" at the beggining of the line for further error checking on the line

	if len(line_string) < 10: # check to see if the line is long enough to be a readable intel hex line
		print "LoadFLASH: BAD Line in file: ", raw_line, "(incomplete intel hex format)"
		return False, 0, 0, 0, 0

	if len(line_string) > max_line_len: # check to see if the line is too long to be a readable intel hex line (Was 255)
		print "LoadFLASH: BAD Line in file: ", raw_line, "(intel hex format not supported, contains too many characters)"
		return False, 0, 0, 0, 0
	
	if len(line_string)%2 != 0: # Check if the line is not divisible by two and is missing characters
		print "LoadFLASH: BAD Line in file: ", raw_line, "(uneven number of characters)"
		return False, 0, 0, 0, 0

	# Verify the integrity of the line with the checksum by adding all the bytes together and verifying the sum is zero
	cont_sum=0 # decimal sum of content bytes used to compute checksum

	for i in xrange(0, len(line_string), 2): # Add all but the last two characters to compute the checksum
		byte=line_string[i:i+2]
		if debug:
			print byte
			print "decimal: ", int(byte,16)#
		cont_sum+=int(byte,16) # Add dec#imal value of the byte to content sum

	line_sum=cont_sum%256 # find the 8 bit remainder to get the 8 least significant bits.
						  # the sum should add up to a multiple of 256 which rolls over to 0.
						  # the content of the line is cancled out through addition to its
						  # two's complement which is the checksum if all data is intact

	checksum = line_string[len(line_string)-2:]	  # checksum written at the end of the line as hex for refference

	if line_sum != 0: # if the content of the line does not add up to zero, there is discrepancy between the data and the checksum
		print "LoadFLASH: BAD Line in file: ", raw_line, "- Content inconsistant with Checksum (", "0x"+checksum, ") Line Sum =", line_sum, "\n"
		return False, 0, 0, 0, 0


	# Line information ******* Variables that are returned
	byte_count = int(line_string[0:2], 16)
	incremental_addr = int(line_string[2:6], 16)
	record_type = int(line_string[6:8], 16)
	data_string = line_string[8:len(line_string)-2]
	# ************************

	if record_type == 0:
		data = array('B', [0]*(byte_count))
		for i in xrange(0,byte_count*2,2):
			data[i/2] = int(data_string[i:i+2],16)
	else:
		data = data_string

	if debug: # If in debug mode, print line information variables
		print "Line: " + line_string
		print "Byte count: ", byte_count
		print "Incremental start_address: ", incremental_addr
		print "Record type: ", record_type
		print "Data bytes: ", data
		print "Stated checksum: ", checksum
		print "Line Sum = ", line_sum

	return True, byte_count, incremental_addr, record_type, data


def write_to_buffer(index, array):
	global target_buffer_size
	global bytes_per_line


	for i in range(index*target_buffer_size, (index*target_buffer_size+target_buffer_size))[::bytes_per_line]:
		# print i%128 #Starting byte of each 8 byte segment of the 128 byte buffer
		buffer_line = '{0:01x}'.format((i%128)/8)
		line = "fww " + buffer_line + " " + create_data_line(i, array)
		WriteCommand(ser, line)



def create_data_line(index, array):
	global bytes_per_line
	line = ""
	if (index-1) <= highest_address:
		for i in range((index+bytes_per_line-1),index-1,-1):
			# print data_array[1]
			try:
				line += '{0:02X}'.format(array[i])
			except IndexError:
				# print i
				# print highest_address
				line += "FF"
	else:
		line = "FFFFFFFFFFFFFFFF"
	return line


def compare_buffers(index, array, read_buf):
	global target_buffer_size
	global bytes_per_line
        
        line,read_buf = read_buf.split("\n", 1)  # Gets rid of the returned command
	for i in range(index*target_buffer_size, (index*target_buffer_size+target_buffer_size))[::bytes_per_line]:
                #print read_buf
		line,read_buf = read_buf.split("\n", 1)  # Splits the buffer based on "new lines" and compares the first segment
                #print "Comparing: ", line.strip(), " to ", create_data_line(i, array), "\n"
		if line.strip() != create_data_line(i, array):
			print "Error, flash data does not match data written to device: "
			print line.strip(), " does not match ", create_data_line(i, array), "\n"
			return 1
	return 0


def WriteCommand(ser,line):
   if line.find("\n") != -1:
       raise cmd("new line in command")
   # write command and make sure the reponse makes sense                                                                                                                                                                                                                          
   ser.write(line)                                                                                                                                                                                                                                                               
#execute the command and read the output                                                                                                                                                                                                                                          
   ser.write("\n")
   rd = ser.read()
#   print "\"", rd, "\" :", hex(ord(rd))
   while rd != "\n":
       rd = ser.read()
#       print "\"", rd, "\" :", hex(ord(rd))
   rd = ser.read()
#   print "\"", rd, "\" :", hex(ord(rd))
   line = ""
   while rd != ">":
       line = line + rd
       rd = ser.read()
#       print "\"", rd, "\" :", hex(ord(rd))
#    line = line.split()                                                                                                                                                                                                                                                          
   return line


def Check_lb_connect():
        ser.write("check")
        if ser.read(5) != "check":
                ser.flushInput()
                ser.write("check")
                if ser.read(5) != "check":
                        print "ERROR: Cannot Connect to the LogicBoard"
                        exit()
        time.sleep(0.01)
        ser.flushInput()
        ser.close
        ser.timeout = None
        ser.open

def Check_tdc_connect():
        ser.close
        ser.timeout = 1
        ser.open
        ser.write("check")
        if ser.read(5) != "check":
                ser.flushInput()
                ser.flushOutput()
                ser.write("check")
                if ser.read(5) != "check":
                        print "ERROR: Cannot Connect to the Integrated TDC"
                        ser.write('\x1D') #exit back to the logicboard
                        exit()
        time.sleep(0.01)
        ser.flushInput()
        ser.close
        ser.timeout = None
        ser.open

def Check_version():
        version_print = WriteCommand(ser, "version")
        #print version_print

        if Inttdc_file:
                if (version_print[:-7] != "g-2 Integrated TDC uC V:") or (int(version_print[-6]) < 1) or (int(version_print[-4:-2]) < 1):
                        print "ERROR: improper version"
                        ser.write('\x1D') #exit back to the logicboard
                        exit()
        if LogicBoard_file:
                if (version_print[:-14] != "g-2 Tracker LogicBoard V:") or (int(version_print[-13:-11]) < 1) or (int(version_print[-10:-8]) < 1) or (int(version_print[-7:-5]) < 1)or (int(version_print[-3:-1]) < 1):
                        print "ERROR: improper version"
                        ser.write('\x1D') #exit back to the logicboard
                        exit()
                        

parser = argparse.ArgumentParser(description='Program flash from intel hex file via serial. Default to 250000 baud rate on ttyUSB0.')

parser.add_argument('input_file', help='Input intel hex file to parse and upload')
parser.add_argument('-c','--check', help='Verify flash after upload.', action='store_true')
parser.add_argument('-i','--inttdc', help='Inttdc board to connect to. Can be 0 or 1.', choices=['0', '1'])
parser.add_argument('-b','--baud', help='Set baudrate. Default is 125000.', type=int, default=125000)
parser.add_argument('-D','--Device', help='Change USB port. Default is /dev/ttyUSB0', default="/dev/ttyUSB0")
args = vars(parser.parse_args())

check_flash = args["check"]
inttdc_board = args["inttdc"]
hex_file = args["input_file"]
baud_rate = args["baud"]
device = args["Device"]


debug = False
default_write = "0xFF"
target_buffer_size = 128 #how many bytes to send before writing to flash
upper_addr = 0 # add the incremental start_address to this to find the actual start_address in memory
start_address = 0
bytes_per_line = 8
max_line_len = 300 # Changes based on file type


# Main Function **********************************************************************************************
file_type = str(hex_file[-3:])
Inttdc_file = 0
Inttdc_uC_file = 0
LogicBoard_file = 0



if file_type == "out":
        Inttdc_file = 1
        if (inttdc_board == None):
                print "usage: programFlash.py [-h] [-c] [-i {0,1}] [-b BAUD] [-D DEVICE] input_file"
                print "ERROR: No inttdc board specified"
                exit()
elif file_type == "hex":
        Inttdc_uC_file = 1
        if (inttdc_board == None):
                print "usage: programFlash.py [-h] [-c] [-i {0,1}] [-b BAUD] [-D DEVICE] input_file"
                print "ERROR: No inttdc board specified"
                exit()
elif file_type == "mcs":
        LogicBoard_file = 1
else:
        print "ERROR: unrecognized filetype"
        exit()

infile=open(hex_file,'r')

highest_address, lowest_address = find_address_bounds(infile)

if highest_address == -1 or lowest_address == -1:
	print "Problem with hex lines"
	raise ValueError("Problem with input file")
	exit()

data_array = array('B', [int(default_write, 16)]*(highest_address+1))

infile=open(hex_file,'r')

for line in infile: # Check integrity of intel hex file lines

	line_check, byte_count, incremental_addr, record_type, data = line_analysis(line) # Pass each line through validation & parsing function

	start_address = upper_addr + incremental_addr

	if record_type == 00:
		data_array[start_address:(start_address+byte_count)] = data[:]

	elif record_type == 04:
 		upper_addr = (int(data,16)<<16)

if debug:
	print "\nHighest Address: ", highest_address
	print "Lowest Address: ", lowest_address
	print "\n"
	print data_array

infile.close()
#print data_array

print "File check completed successfully"

line = ""
counter = 0
buffers = ((highest_address+1) / target_buffer_size)
percentage = 0
if highest_address % target_buffer_size:
	buffers += 1
buffer_chunks = target_buffer_size/bytes_per_line

#open the serial port
# ser = serial.Serial("/dev/ttyUSB1",115200);
ser = serial.Serial(device, baud_rate);
ser.timeout = 1
ser.flushInput()
ser.open

Check_lb_connect()

if Inttdc_file:
        print "Connecting to Integrated TDC"
        WriteCommand(ser, "")
        WriteCommand(ser, "")
        WriteCommand(ser, "")
        ser.write("tdc_shell ")
        ser.write(inttdc_board)
        ser.write("\n")
        time.sleep(0.01)
elif Inttdc_uC_file:
        print "Connecting to Integrated TDC uC"
        WriteCommand(ser, "")
        WriteCommand(ser, "")
        WriteCommand(ser, "")
        ser.flushInput()
        connect_to_inttdc(inttdc_board, ser)
        get_inttdc_info(ser);
        tdc_data = check_hex_file(hex_file, ser);
        program_inttdc_uC_flash(tdc_data, ser);
        leave_bootloader(ser);
        exit()
elif LogicBoard_file:
        print "Connecting to LogicBoard"

ser.flushInput()

Check_tdc_connect()

print "Check version"
WriteCommand(ser, "")
Check_version()


print "Starting to write to device"
                
print "Erase flash"
WriteCommand(ser, "fe")
print "Erase done"
WriteCommand(ser, "fc")
WriteCommand(ser, "faw 000000")
print "i from 0 to " + str(buffers)
print "0%"
for i in range(buffers):
	write_to_buffer(i, data_array)
	WriteCommand(ser, "fw")
        if i&0x1FF == 0x100:
                print str(100.0*i/(buffers*1.0)) + "%"

print "Completed writing to device"
if Inttdc_file:
        WriteCommand(ser, "conf_fpga")
        ser.write('\x1D') #exit back to the logicboard
        
if check_flash:
        print "Verifying Flash"
        WriteCommand(ser, "faw 000000")
        readback = []
        mem_problem = 0

        for i in range(buffers):
                # if ((counter%target_buffer_size) == 0):
                WriteCommand(ser, "fr") # Read flash memory to buffer
                read_back = WriteCommand(ser, "fwr") # Read flash memory back to compare
                mem_problem = mem_problem | compare_buffers(i, data_array, read_back)
        if mem_problem:
                print "Error: Memory not consistent with input file"
        else:
                print "Flash verified and is consistent with input file"
